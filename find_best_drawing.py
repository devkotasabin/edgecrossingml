from input_functions import *

my_inf = 1000000000

def plot_data_from_file(file_name):
 global my_inf
 with open(file_name) as f:
  data = json.load(f)
 #print('length of data:'+str(len(data)))
 #print('length of data row:'+str(len(data[0])))
 #print(data)
 min_property = [my_inf for i in range(len(data[0]))]
 max_property = [-1 for i in range(len(data[0]))]
 min_index = [0 for i in range(len(data[0]))]
 max_index = [0 for i in range(len(data[0]))]
 for i in range(len(data)):
  for j in range(len(data[0])):
   if data[i][j]<min_property[j]:
    min_property[j]=data[i][j]
    min_index[j] = i
   if data[i][j]>max_property[j]:
    max_property[j]=data[i][j]
    max_index[j] = i

 return max_property, max_index

def write_array(arr, file_name):
 file = open(file_name,"w")
 for j in range(len(arr)):
  file.write(str(j+1)+"\t"+str(arr[j])+"\n")
 file.close()

def find_max_and_convert_file(folder_name):
 number_of_inputs = 16
 neato_property, neato_index = plot_data_from_file(folder_name+'/neato_min_angles.txt')
 sfdp_property, sfdp_index = plot_data_from_file(folder_name+'/sfdp_min_angles.txt')
 write_array(neato_property, folder_name+'/neato_max_min_angle.txt')
 write_array(sfdp_property, folder_name+'/sfdp_max_min_angle.txt')
 for i in range(number_of_inputs):
  dot_to_json(folder_name+'/neato_layout'+str(i+1)+'_drawing_'+str(neato_index[i]+1)+'.dot', folder_name+'/neato'+str(i+1)+'.json')
  dot_to_json(folder_name+'/sfdp_layout'+str(i+1)+'_drawing_'+str(sfdp_index[i]+1)+'.dot', folder_name+'/sfdp'+str(i+1)+'.json')


find_max_and_convert_file('../graphs/input18')

