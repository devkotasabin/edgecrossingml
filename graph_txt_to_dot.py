import sys
from input_functions import *

if len(sys.argv) < 3:
 print("usage: python3 graph_generator.py input_file.txt/input_file_pattern output_file.dot/output_file_pattern")
 print()
 quit()

#take input
output_file = sys.argv[2]
#file = open(sys.argv[1],"r")

#n, coord_list, edge_list = take_input(sys.argv[1])

#print("vertices:")
#print(n)
#print("coordinates:")
#print(coord_list)
#print("edges:")
#print(edge_list)

#from graphviz import Graph
import pygraphviz as gv

#G = Graph(engine='neato')
#for [u,v] in edge_list:
# G.edge(str(u),str(v))

#print(G.pipe().decode('utf-8'))
#print(G)
#file2 = open(output_file, 'w')
#file2.write(str(G))
#file2.close()

#G.layout(D,'dot')
#G.render()
#u = G.findnode('0')
#pos = G.getv(u, "pos")
#print(pos)

#G.render()
#print(G)

#import networkx as nx

#G = nx.Graph()
#for [u,v] in edge_list:
# G.add_edge(u,v)

#pos = nx.nx_agraph.graphviz_layout(G, prog='dot')
#print(pos) 

#from networkx.drawing.nx_agraph import write_dot

#write_dot(G,output_file)

def convert_all_files():
 for i in range(1,101):
  n, coord_list, edge_list = take_input(sys.argv[1]+str(i)+'.txt')
  G = gv.Graph()
  for [u,v] in edge_list:
   G.edge(str(u),str(v))
  file2 = open(output_file+str(i)+'.dot', 'w')
  file2.write(str(G))
  file2.close()

convert_all_files()

