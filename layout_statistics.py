import json
import sys
from edge_crossing import *
from input_functions import *
from utils import *
import time


import poly_point_isect
from scipy.special import comb


my_inf = 10000000


def make_segment(p1x, p1y, p2x, p2y):
 segment = (p1x, p1y)
 segment = (segment,) + ((p2x, p2y),)
 return segment

def number_of_intersections(file_name):
 global my_inf 
 print(file_name)
 if file_name.endswith('.dot'):
  n, node_coords, edge_list = take_input_from_dot(file_name)
 elif file_name.endswith('.txt'):
  n, node_coords, edge_list = take_input(file_name)
 count = 0
 segment_arr = []
 for i in range(len(edge_list)):
  edge1 = edge_list[i]
  segment_arr.append(make_segment(node_coords[edge1[0]][0], node_coords[edge1[0]][1], node_coords[edge1[1]][0], node_coords[edge1[1]][1]))
 #print(segment_arr)
 try:
  isect = poly_point_isect.isect_segments_include_segments(segment_arr)
 except KeyError:
  print("Oops!  That was a key error.  Try again...")
 finally:
  return my_inf
 for x in isect:
  intersection, segment_pair = x
  #seg1, seg2 = segment_pair
  seg1 = segment_pair[0]
  seg2 = segment_pair[1]
  p1, q1 = seg1
  p1x, p1y = p1
  q1x, q1y = q1
  p2, q2 = seg2
  p2x, p2y = p2
  q2x, q2y = q2
  if(doIntersect(p1x, p1y, q1x, q1y, p2x, p2y, q2x, q2y)):
   if len(segment_pair)==2:
    count = count + 1
   else:
    count = count + comb(len(segment_pair), 2)
 #print(isect)
 return count


# this function used to find out whether two vertices share or not
# but now it is redundant as the computation is already placed in edgecrossing.py inside the doIntersect function
def share_vertex(e1, e2):
 if e1[0]==e2[0] or e1[0]==e2[1]:
  return True
 if e1[1]==e2[0] or e1[1]==e2[1]:
  return True
 return False


def number_of_intersections_simple(file_name):
 #print(file_name)
 if file_name.endswith('.dot'):
  node_coords, edge_list = parse_dot_file(file_name)
 elif file_name.endswith('.txt'):
  n, node_coords, edge_list = take_input(file_name)
 count = 0
 for i in range(len(edge_list)):
  for j in range(i+1,len(edge_list)):
   edge1 = edge_list[i]
   edge2 = edge_list[j]
   #if not share_vertex(edge1, edge2):
   if(doIntersect(node_coords[edge1[0]][0], node_coords[edge1[0]][1], node_coords[edge1[1]][0], node_coords[edge1[1]][1], node_coords[edge2[0]][0], node_coords[edge2[0]][1], node_coords[edge2[1]][0], node_coords[edge2[1]][1])):
    count = count + 1
    #print(str(edge1)+" intersects "+str(edge2))
 return count

def all_graph_crossing_numbers(algo_name, folder_name, number_of_graphs, drawing):
 arr = []
 for i in range(1,number_of_graphs+1):
 #for i in range(1,3):
  #print(folder_name+'/'+algo_name+'_layout'+str(i)+'.dot')
  #print('number of intersections:')
  #print(number_of_intersections(folder_name+'/'+algo_name+'_layout'+str(i)+'.dot'))
  if drawing!=-1:
   arr.append(number_of_intersections(folder_name+'/'+algo_name+'_layout'+str(i)+'_drawing_'+str(drawing)+'.dot'))
  else:
   arr.append(number_of_intersections(folder_name+'/input'+str(i)+'.txt'))
 return arr

def all_drawings_crossings(algo_name, folder_name, number_of_graphs, number_of_drawings):
 arr = []
 for i in range(1, number_of_drawings+1):
  arr.append(all_graph_crossing_numbers(algo_name, folder_name, number_of_graphs, i))
 return arr

def all_graph_min_angle(algo_name, folder_name, number_of_graphs, drawing):
 arr = []
 for i in range(1,number_of_graphs+1):
  if drawing!=-1:
   arr.append(min_angle(folder_name+'/'+algo_name+'_layout'+str(i)+'_drawing_'+str(drawing)+'.dot'))
  else:
   arr.append(min_angle(folder_name+'/input'+str(i)+'.txt'))
 return arr

def all_drawings_min_angles(algo_name, folder_name, number_of_graphs, number_of_drawings):
 arr = []
 for i in range(1, number_of_drawings+1):
  arr.append(all_graph_min_angle(algo_name, folder_name, number_of_graphs, i))
 return arr

def usage():
 if len(sys.argv)<4:
  print('usage:python layout_statistics.py folder_name number_of_graphs number_of_drawings')
  quit()
 else:
  return sys.argv[1], int(sys.argv[2]), int(sys.argv[3])

#file = open('sfdp_crossings2.txt','w')
#file.write(json.dumps(all_graph_crossing_numbers('sfdp', 'erdos_renyi2')))
#file.close()

#file = open('neato_crossings2.txt','w')
#file.write(json.dumps(all_graph_crossing_numbers('neato', 'erdos_renyi2')))
#file.close()

#print(json.dumps(number_of_intersections('test_graphs/sfdp_layout_complete5.dot')))

folder_name, number_of_graphs, number_of_drawings = usage()

start_time = time.time()
file = open(folder_name+'/'+'sfdp_crossings.txt','w')
file.write(json.dumps(all_drawings_crossings('sfdp', folder_name, number_of_graphs, number_of_drawings)))
file.close()
print('Time to complete all_drawings_crossings sfdp: ' + str((time.time() - start_time)) + ' seconds')

start_time = time.time()
file = open(folder_name+'/'+'neato_crossings.txt','w')
file.write(json.dumps(all_drawings_crossings('neato', folder_name, number_of_graphs, number_of_drawings)))
file.close()
print('Time to complete all_drawings_crossings neato: ' + str((time.time() - start_time)) + ' seconds')

start_time = time.time()
file = open(folder_name+'/'+'input_crossings.txt','w')
file.write(json.dumps(all_graph_crossing_numbers('', folder_name, number_of_graphs, -1)))
file.close()
print('Time to complete input crossings: ' + str((time.time() - start_time)) + ' seconds')

start_time = time.time()
file = open(folder_name+'/'+'sfdp_min_angles.txt','w')
file.write(json.dumps(all_drawings_min_angles('sfdp', folder_name, number_of_graphs, number_of_drawings)))
file.close()
print('Time to complete all_drawings_min_angles sfdp: ' + str((time.time() - start_time)) + ' seconds')

start_time = time.time()
file = open(folder_name+'/'+'neato_min_angles.txt','w')
file.write(json.dumps(all_drawings_min_angles('neato', folder_name, number_of_graphs, number_of_drawings)))
file.close()
print('Time to complete all_drawings_min_angles neato: ' + str((time.time() - start_time)) + ' seconds')

start_time = time.time()
file = open(folder_name+'/'+'input_min_angles.txt','w')
file.write(json.dumps(all_graph_min_angle('', folder_name, number_of_graphs, -1)))
file.close()
print('Time to complete input min_angles: ' + str((time.time() - start_time)) + ' seconds')

