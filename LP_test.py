from LP_with_input import *

# #Input 19
# #T-shape with unequal edge length
ax = 0
ay = 30

bx = 0
by = -30

cx = -2
cy = 25

dx = 5
dy = 25

# to get ux
print(get_ux(ax, ay, bx, by, cx, cy, dx, dy))
# to get uy
print(get_uy(ax, ay, bx, by, cx, cy, dx, dy))
# to get gamma
print(get_gamma(ax, ay, bx, by, cx, cy, dx, dy))

