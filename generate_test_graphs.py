import networkx as nx
import random


file = open("test_graphs/complete4.txt","w")
graph = nx.complete_graph(4)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("test_graphs/complete5.txt","w")
graph = nx.complete_graph(5)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("test_graphs/complete6.txt","w")
graph = nx.complete_graph(6)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("test_graphs/complete7.txt","w")
graph = nx.complete_graph(7)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("test_graphs/path5.txt","w")
graph = nx.path_graph(5)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("test_graphs/cycle6.txt","w")
graph = nx.cycle_graph(6)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("er_small_graphs/er_15_0.5.txt","w")
graph = nx.generators.random_graphs.erdos_renyi_graph(15, 0.5)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("er_small_graphs/er_15_0.4.txt","w")
graph = nx.generators.random_graphs.erdos_renyi_graph(15, 0.4)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("er_small_graphs/er_8_0.6.txt","w")
graph = nx.generators.random_graphs.erdos_renyi_graph(8, 0.6)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("er_small_graphs/er_17_0.4.txt","w")
graph = nx.generators.random_graphs.erdos_renyi_graph(17, 0.4)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("er_small_graphs/er_20_0.4.txt","w")
graph = nx.generators.random_graphs.erdos_renyi_graph(20, 0.4)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()


file = open("er_small_graphs/er_10_0.6.txt","w")
graph = nx.generators.random_graphs.erdos_renyi_graph(10, 0.6)
file.write(str(graph.number_of_nodes())+"\n");
for j in range(graph.number_of_nodes()):
 file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
edges = graph.edges()
for e in edges:
 file.write(str(e[0])+" "+str(e[1])+"\n")
file.close()

