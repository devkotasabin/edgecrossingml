import numpy as np
from input_functions import *
from utils import *

#filename_init = 'test_graphs/cycle20'
#filename_init = 'test_graphs/tree15'
filename_init = 'test_graphs/tree127'
filename_xy = filename_init + '_xy__dummy.npy'
filename_txt = filename_init + '.txt'
filename_img = filename_init + '.png'
X_new = np.load(filename_xy)
W, K, NUM_RUNS, n, d = X_new.shape
arr = X_new[W-1][K-1][NUM_RUNS-1]
print(arr)
print(arr[:,0])
print(arr[:,1])
n, coord_list, edge_list = take_input(filename_txt)
draw_graph(arr[:,0], arr[:,1], edge_list, filename_img)


