import numpy as np
import os
from input_functions import *

import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

def write_array(arr, file_name):
 file = open(file_name,"w")
 for j in range(len(arr)):
  file.write(str(j+1)+"\t"+str(arr[j])+"\n")
 file.close()

def check_init_layout(edge_threshold, file_names, iterations_small_graph, iterations_large_graph):
 W_start = 1
 W_end = 2

 K_start = -5
 K_end = 5

 NUM_RUNS = 15
 NUM_ITERATIONS = iterations_small_graph
 #job_name_prefix = 'edge_crossings'
 GD_OPTIONS = ['VANILLA', 'MOMENTUM', 'NESTEROV', 'ADAGRAD', 'RMSPROP', 'ADAM']
 alpha_arr = ['1e-3', '1e-3', '1e-3', '1e-1', '1e-1', '1e-1'] # learning rate

 max_min_angle = []

 neato_wins = 0
 sfdp_wins = 0
 random_wins = 0

 #for n in range(2):
 for n in range(1):
  for W in range(W_start, W_end):
   for i in range(len(file_names)):
    max_min_angle.append(-1)
    dummy, coord_list, edge_list = take_input(file_names[i]+'.txt')
    print(file_names[i]+'.txt')
    current_winner_neato = False
    current_winner_sfdp = False
    current_winner_random = False
    neato_angle = 0
    sfdp_angle = 0
    random_angle = 0
    for s in range(len(GD_OPTIONS)):
     if len(edge_list)>edge_threshold:
      NUM_ITERATIONS = iterations_large_graph
      K_start = 1
      K_end = 2
      layout = ['neato', 'sfdp', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '0', '0']
     else:
      NUM_ITERATIONS = iterations_small_graph
      K_start = -5
      K_end = 5
      layout = ['neato', 'neato', 'neato', 'neato', 'neato', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'random', 'random', 'random', 'random', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '1', '2', '3', '4', '0', '1', '2', '3', '4', '0', '1', '2', '3', '4']
     K_start_str = str(K_start)
     if K_start<0:
      K_start_str = 'N'+str(-K_start)
     K_end_str = str(K_end)
     if K_end<0:
      K_end_str = 'N'+str(-K_end)
     for r in range(0,NUM_RUNS):
      file_ext = 'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'_STRAT_'+GD_OPTIONS[s]+'_'+layout[r]+'_'+layout_number[r]
      if not os.path.exists(file_names[i] + '_ang_'+file_ext+'.npy'):
       continue
      MIN_ANGLES = np.load(file_names[i] + '_ang_'+file_ext+'.npy')
      X_new = np.load(file_names[i] + '_xy_'+file_ext+'.npy')
      for K in range(K_start,K_end):
       for it in range(NUM_ITERATIONS):
        if max_min_angle[i]<MIN_ANGLES[W-W_start][K-K_start][it]:
         max_min_angle[i] = MIN_ANGLES[W-W_start][K-K_start][it]
         if 'neato' in file_ext:
          current_winner_neato = True
          neato_angle = max_min_angle[i]
          if neato_angle > sfdp_angle:
           current_winner_sfdp = False
          if neato_angle > random_angle:
           current_winner_random = False
         elif 'sfdp' in file_ext:
          current_winner_sfdp = True
          sfdp_angle = max_min_angle[i]
          if sfdp_angle > neato_angle:
           current_winner_neato = False
          if sfdp_angle > random_angle:
           current_winner_random = False
         else:
          current_winner_random = True
          random_angle = max_min_angle[i]
          if random_angle > neato_angle:
           current_winner_neato = False
          if random_angle > sfdp_angle:
           current_winner_sfdp = False
    if current_winner_neato:
     neato_wins += 1
    if current_winner_sfdp:
     sfdp_wins += 1
    if current_winner_random:
     random_wins += 1

 return [neato_wins, sfdp_wins, random_wins]

def draw_bar_chart(objects, y_pos, performance, ylabel, title, output_file):
 plt.bar(y_pos, performance, align='center', alpha=0.5)
 plt.xticks(y_pos, objects)
 plt.ylabel(ylabel)
 if not title == '':
  plt.title(title)
 
 #plt.show()
 plt.savefig(output_file)
 plt.close()

def check_param_k(edge_threshold, file_names, iterations_small_graph, iterations_large_graph, specific_layout):
 W_start = 1
 W_end = 2

 K_start = -5
 K_end = 5

 NUM_RUNS = 15
 NUM_ITERATIONS = iterations_small_graph
 #job_name_prefix = 'edge_crossings'
 GD_OPTIONS = ['VANILLA', 'MOMENTUM', 'NESTEROV', 'ADAGRAD', 'RMSPROP', 'ADAM']
 alpha_arr = ['1e-3', '1e-3', '1e-3', '1e-1', '1e-1', '1e-1'] # learning rate

 max_min_angle = []

 k_wins = [0]*(K_end - K_start)

 for n in range(1):
  for W in range(W_start, W_end):
   for i in range(len(file_names)):
    max_min_angle.append(-1)
    dummy, coord_list, edge_list = take_input(file_names[i]+'.txt')
    current_winner = [False]*(K_end - K_start)
    #current_winner_neato = False
    #current_winner_sfdp = False
    #current_winner_random = False
    k_angle = [0]*(K_end - K_start)
    #neato_angle = 0
    #sfdp_angle = 0
    #random_angle = 0
    for s in range(len(GD_OPTIONS)):
     if len(edge_list)>edge_threshold:
      NUM_ITERATIONS = iterations_large_graph
      K_start = 1
      K_end = 2
      layout = ['neato', 'sfdp', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '0', '0']
     else:
      NUM_ITERATIONS = iterations_small_graph
      K_start = -5
      K_end = 5
      layout = ['neato', 'neato', 'neato', 'neato', 'neato', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'random', 'random', 'random', 'random', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '1', '2', '3', '4', '0', '1', '2', '3', '4', '0', '1', '2', '3', '4']
     K_start_str = str(K_start)
     if K_start<0:
      K_start_str = 'N'+str(-K_start)
     K_end_str = str(K_end)
     if K_end<0:
      K_end_str = 'N'+str(-K_end)
     for r in range(0,NUM_RUNS):
      if not specific_layout == '':
       if not specific_layout in layout[r]:
        continue
      file_ext = 'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'_STRAT_'+GD_OPTIONS[s]+'_'+layout[r]+'_'+layout_number[r]
      #if not os.path.exists(file_names[i] + '_ang_'+file_ext+'.npy'):
      # continue
      MIN_ANGLES = np.load(file_names[i] + '_ang_'+file_ext+'.npy')
      X_new = np.load(file_names[i] + '_xy_'+file_ext+'.npy')
      for K in range(K_start,K_end):
       for it in range(NUM_ITERATIONS):
        if max_min_angle[i]<MIN_ANGLES[W-W_start][K-K_start][it]:
         max_min_angle[i] = MIN_ANGLES[W-W_start][K-K_start][it]
         current_winner[K] = True
         k_angle[K] = max_min_angle[i]
         for ki in range(K_start,K_end):
          if k_angle[ki] > k_angle[K]:
           current_winner[ki] = False
    for ki in range(K_start,K_end):
     if current_winner[ki]:
      k_wins[ki] += 1

 return k_wins

def check_sgd(edge_threshold, file_names, iterations_small_graph, iterations_large_graph, specific_layout):
 W_start = 1
 W_end = 2

 K_start = -5
 K_end = 5

 NUM_RUNS = 15
 NUM_ITERATIONS = iterations_small_graph
 #job_name_prefix = 'edge_crossings'
 GD_OPTIONS = ['VANILLA', 'MOMENTUM', 'NESTEROV', 'ADAGRAD', 'RMSPROP', 'ADAM']
 alpha_arr = ['1e-3', '1e-3', '1e-3', '1e-1', '1e-1', '1e-1'] # learning rate

 max_min_angle = []

 sgd_wins = [0]*len(GD_OPTIONS)

 for n in range(1):
  for W in range(W_start, W_end):
   for i in range(len(file_names)):
    max_min_angle.append(-1)
    dummy, coord_list, edge_list = take_input(file_names[i]+'.txt')
    current_winner = [False]*len(GD_OPTIONS)
    sgd_angle = [0]*len(GD_OPTIONS)
    for s in range(len(GD_OPTIONS)):
     if len(edge_list)>edge_threshold:
      NUM_ITERATIONS = iterations_large_graph
      K_start = 1
      K_end = 2
      layout = ['neato', 'sfdp', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '0', '0']
     else:
      NUM_ITERATIONS = iterations_small_graph
      K_start = -5
      K_end = 5
      layout = ['neato', 'neato', 'neato', 'neato', 'neato', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'random', 'random', 'random', 'random', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '1', '2', '3', '4', '0', '1', '2', '3', '4', '0', '1', '2', '3', '4']
     K_start_str = str(K_start)
     if K_start<0:
      K_start_str = 'N'+str(-K_start)
     K_end_str = str(K_end)
     if K_end<0:
      K_end_str = 'N'+str(-K_end)
     for r in range(0,NUM_RUNS):
      if not specific_layout == '':
       if not specific_layout in layout[r]:
        continue
      file_ext = 'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'_STRAT_'+GD_OPTIONS[s]+'_'+layout[r]+'_'+layout_number[r]
      #if not os.path.exists(file_names[i] + '_ang_'+file_ext+'.npy'):
      # continue
      MIN_ANGLES = np.load(file_names[i] + '_ang_'+file_ext+'.npy')
      X_new = np.load(file_names[i] + '_xy_'+file_ext+'.npy')
      for K in range(K_start,K_end):
       for it in range(NUM_ITERATIONS):
        if max_min_angle[i]<MIN_ANGLES[W-W_start][K-K_start][it]:
         max_min_angle[i] = MIN_ANGLES[W-W_start][K-K_start][it]
         current_winner[s] = True
         sgd_angle[s] = max_min_angle[i]
         for si in range(len(GD_OPTIONS)):
          if sgd_angle[si] > sgd_angle[s]:
           current_winner[si] = False
    for si in range(len(GD_OPTIONS)):
     if current_winner[si]:
      sgd_wins[si] += 1

 return sgd_wins


#check_grad_desc(16, 100)
#check_grad_desc(10, 100)

#check_grad_desc(11, 100, '../graphs/input_angle/')
#check_grad_desc(16, 100, '../graphs/input_angle_17/')
#check_grad_desc_ncr(11, 100, '../graphs/input_crossing/')
#check_grad_desc_ncr(16, 100, '../graphs/input_crossing_17/')

#check_grad_desc(11, 100, '../graphs/input_angle_vanila/')
#check_grad_desc(14, 100, '../graphs/input_angle_new_GDs/', 30, 2)

def get_file_names():
 file_names = ["input1", "input2", "input3", "input4", "input5", "input6", "input1", "input2", "input3", "input4", "input5", "input6", "input7"]
 for i in range(6):
  file_names[i] = "../graphs/input_angle_new_GDs_17/" + file_names[i]
 for i in range(6, 13):
  file_names[i] = "../graphs/input_angle_new_GDs/" + file_names[i]

 return file_names


#objects = ('Neato', 'SFDP', 'Random')
#performance = check_init_layout(100, get_file_names(), 30, 2)
#draw_bar_chart(objects, np.arange(len(objects)), performance, 'Number of wins', '', '../plots/init_layout.png')

#objects = ('1/32', '1/16', '1/8', '1/4', '1/2', '1', '2', '4', '8', '16')
#performance = check_param_k(100, get_file_names(), 30, 2, '')
#draw_bar_chart(objects, np.arange(len(objects)), performance, 'Number of wins', '', '../plots/param_K.png')

#performance = check_param_k(100, get_file_names(), 30, 2, 'neato')
#draw_bar_chart(objects, np.arange(len(objects)), performance, 'Number of wins', '', '../plots/param_K_neato.png')
#performance = check_param_k(100, get_file_names(), 30, 2, 'sfdp')
#draw_bar_chart(objects, np.arange(len(objects)), performance, 'Number of wins', '', '../plots/param_K_sfdp.png')
#performance = check_param_k(100, get_file_names(), 30, 2, 'random')
#draw_bar_chart(objects, np.arange(len(objects)), performance, 'Number of wins', '', '../plots/param_K_random.png')

objects = ('VANILLA', 'MOMENTUM', 'NESTEROV', 'ADAGRAD', 'RMSPROP', 'ADAM')
performance = check_sgd(100, get_file_names(), 30, 2, '')
draw_bar_chart(objects, np.arange(len(objects)), performance, 'Number of wins', '', '../plots/sgd_comparison.png')




