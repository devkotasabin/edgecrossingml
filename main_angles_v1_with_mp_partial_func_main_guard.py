import networkx as nx
import numpy as np
from numpy import inf
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from LP_with_input import *
from edge_crossing import *
import math
from input_functions import *
import multiprocessing
from multiprocessing import Pool
from functools import partial

if __name__ == '__main__':

	# print "Everything inside main"

	NUM_PROCESSES = multiprocessing.cpu_count()

	EPSILON = 0.000001
	# K = 100000
	K = 16
	# W = 0
	W = 1

	USE_NEATO_INITIAL = True
	USE_INITIAL_NODE_COORDS = False

	OPTIMIZE_CROSSING_ANGLE = True

	###### Initialize and load the graphs; Compute the weights and distances

	# G = nx.petersen_graph()
	# G = nx.complete_graph(6)
	# G = nx.complete_graph(7)
	# G = nx.wheel_graph(9)

	FILENAME = 'meeting_08_21_2018/input/input1'

	G = build_networkx_graph(FILENAME + '.txt')

	dummy1, init_node_coords, dummy2 = take_input(FILENAME + '.txt')

	n = G.number_of_nodes()
	m = G.number_of_edges()
	edge_list = G.edges()

	distances = nx.floyd_warshall(G)

	# Initialize the coordinates randomly in the range [-50, 50]
	X_curr = np.random.rand(n,2)*100 - 50

	if USE_NEATO_INITIAL:
		# pos = nx.nx_agraph.graphviz_layout(G)
		pos = nx.nx_agraph.graphviz_layout(G, args = '-Gstart=rand')
		# Copy the coordinates from pos to X_curr
		for i in range(0,n):
			X_curr[i] = pos[i]

	if USE_INITIAL_NODE_COORDS:
		# Copy the coordinates from initial coords to X_curr
		for i in range(0,n):
			X_curr[i] = init_node_coords[i]



	# Z=np.copy(X_curr)
	X_prev = np.copy(X_curr)

	# Copy the distances into a 2D numpy array
	distances = np.array([[distances[i][j] for j in distances[i]] for i in distances])
	# weights = 1/(d^2)
	weights = 1/pow(distances,2)
	weights[weights == inf] = 0


	# Define: penalties, u_params, gammas, edgesID


	# penalties: a 2D array containing the penalties for each possible edge pair
	# For now the penalties start with 0 and gradually increase by 1 in the next iteration
	# if the crossing persists.

	penalties = np.zeros((m, m))

	# u_params: a 3D array containing the u vectors for each edge pair
	u_params = np.zeros((m, m, 2))

	# gammas: a 2D array containing the gamma values for each possible edge pair
	gammas = np.zeros((m, m))

	# all these variables need to be accessed as a 2D array
	# with the edge pair as the i,j index of the 2D array.

	#The 2D array is of size M*M where M is the number of edges. Max edges = n*(n-1)/2
	# for complete graph



#This function returns the nodes of an edge given its index in the edge list
def getNodesforEdge(index):
	return edge_list[index][0], edge_list[index][1]


# This function extracts the edge pair in the form of matrices 
# Returns two matrices A and B
# A contains [a1x, a1y; a2x a2y] 
# B contains [b1x, b1y; b2x b2y]
def getEdgePairAsMatrix(X,i,j):
	A = np.zeros((2,2))
	B = np.zeros((2,2))
	
	i1, i2 = getNodesforEdge(i)
	j1, j2 = getNodesforEdge(j)

	A[0,:] = X[i1, :]
	A[1,:] = X[i2, :]

	B[0,:] = X[j1, :]
	B[1,:] = X[j2, :]

	return A,B


def plotGraphandStats(X):
	print(X)
	# plt.scatter(X[:,0], X[:,1], color='red')

	# for every edge in the graph
	# draw a line joining the endpoints
	# for i in range(0,m):
	# 	i1, i2 = getNodesforEdge(i)
	# 	A = np.zeros((2,2))

	# 	A[0,:] = X[i1, :]
	# 	A[1,:] = X[i2, :]
	# 	plt.plot(A[:,0] , A[:,1], color='blue')

	num_intersections = 0
	min_angle = math.pi/2.0

	print "Number of edges: ", m

	# loop through all edge pairs
	for i in range(0,m):
		for j in range(i+1,m):

			A,B = getEdgePairAsMatrix(X,i,j)
			# doIntersect(x11, y11, x12, y12, x21, y21, x22, y22)
			if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
				
				# print A
				# print B
				# print i
				# print j
				x_pt, y_pt = getIntersection(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
				theta = getAngleLineSeg(A[0][0], A[0][1], B[0][0], B[0][1], x_pt, y_pt)
				if theta > math.pi/2.0:
					theta = math.pi - theta
				if theta < min_angle:
					min_angle = theta

				num_intersections += 1

	print "Number of Edge Crossings: ", num_intersections
	print "Minimum Angle: ", to_deg(min_angle)

	# plt.show()


# This function computes the stress of an embedding. It takes as input the coordinates X, 
# weights (i.e. d_{ij}^(-2)), ideal distances between the nodes, and the number of nodes
# in the graph 

def parFunc1(X, weights, distances, n, i):
	s = 0
	for j in range(i+1,n):
		norm = X[i,:] - X[j,:]
		norm = np.sqrt(sum(pow(norm,2)))
		s += weights[i,j] * pow((norm - distances[i,j]), 2)
	return s

def stress(X, weights, distances, n):
	# print "Parameters:", X, type(X)
	s = 0.0

	# PAR: Parallelizing the outer loop
	# for i in range(0,n):
	# 	for j in range(i+1,n):
	# 		norm = X[i,:] - X[j,:]
	# 		norm = np.sqrt(sum(pow(norm,2)))
	# 		s += weights[i,j] * pow((norm - distances[i,j]), 2)
	# return s

	# def parFunc1(i):
	# 	s = 0
	# 	for j in range(i+1,n):
	# 		norm = X[i,:] - X[j,:]
	# 		norm = np.sqrt(sum(pow(norm,2)))
	# 		s += weights[i,j] * pow((norm - distances[i,j]), 2)
	# 	return s		

	partialFunc1 = partial(parFunc1, X, weights, distances, n)
	pool = Pool(processes = NUM_PROCESSES)
	s = pool.map(partialFunc1,range(n))
	pool.close() # no more tasks
	pool.join()  # wrap up current tasks
	s = sum(s)
	return s


# This function computes the stress of an embedding X. It needs weights, ideal distances, 
# and number of nodes already initialized
def stress_X(X):
	s = 0.0
	for i in range(0,n):
		for j in range(i+1,n):
			norm = X[i,:] - X[j,:]
			norm = np.sqrt(sum(pow(norm,2)))
			s += weights[i,j] * pow((norm - distances[i,j]), 2)
	return s


# This function computes the modified objective function i.e. a sum of stress and penalty function
def modified_cost(X):
	#Reshape the 1D array to a n*2 matrix
	X = X.reshape((n,2))
	return (W*stress(X, weights, distances, n)) + K*sum_penalty(X)

def max_zero(a):
	return np.maximum(0,a)

def parFunc2(X, i):
	sumPenalty = 0.0
	for j in range(i+1,m):

		# Add the penalty
		# ||(-Au - eY)+||1 + ||(Bu + (1 + Y)e)+||1
		# z_+ = max(0,z)

		# sumPenalty += penalty_ij/2 * [||(-Ai(X)ui - Yie)+||1
		# + ||(Bi(X)ui + (1 + Yi)e)+||1] * cos^2(theta)

		A,B = getEdgePairAsMatrix(X,i,j)
		if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
			# x_pt, y_pt = getIntersection(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
			# theta = getAngleLineSeg(A[0][0], A[0][1], B[0][0], B[0][1], x_pt, y_pt)
			
			# cos(theta) can be computed directly using the dot product between vectors
			# a = (A[1][0] - A[0][0], A[1][1] - A[0][1])
			# b = (B[1][0] - B[0][0], B[1][1] - B[0][1])
			# cos(theta) =  a.b / (||a||*||b||)
			aVec = A[1,:] - A[0,:]
			bVec = B[1,:] - B[0,:]
			cos_sq_theta = np.dot(aVec, bVec)**2
			cos_sq_theta = cos_sq_theta / (np.dot(aVec, aVec))
			cos_sq_theta = cos_sq_theta / (np.dot(bVec, bVec))

			# sumPenalty += (penalties[i][j]/2.0) * (math.cos(theta)**2) * (np.sum(max_zero(-np.matmul(A,u_params[i][j])- gammas[i][j] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[i][j])+ (1+gammas[i][j]) * np.array([1,1]))))
			sumPenalty += (penalties[i][j]/2.0) * (cos_sq_theta) * (np.sum(max_zero(-np.matmul(A,u_params[i][j])- gammas[i][j] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[i][j])+ (1+gammas[i][j]) * np.array([1,1]))))
	return sumPenalty

def sum_penalty(X):
	sumPenalty = 0.0

	# PAR: Parallelizing the outer loop
	# loop through all edge pairs
	# for i in range(0,m):
	# 	for j in range(i+1,m):

	# 		# Add the penalty
	# 		# ||(-Au - eY)+||1 + ||(Bu + (1 + Y)e)+||1
	# 		# z_+ = max(0,z)

	# 		# sumPenalty += penalty_ij/2 * [||(-Ai(X)ui - Yie)+||1
	# 		# + ||(Bi(X)ui + (1 + Yi)e)+||1] * cos^2(theta)

	# 		A,B = getEdgePairAsMatrix(X,i,j)
	# 		if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
	# 			# x_pt, y_pt = getIntersection(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
	# 			# theta = getAngleLineSeg(A[0][0], A[0][1], B[0][0], B[0][1], x_pt, y_pt)
				
	# 			# cos(theta) can be computed directly using the dot product between vectors
	# 			# a = (A[1][0] - A[0][0], A[1][1] - A[0][1])
	# 			# b = (B[1][0] - B[0][0], B[1][1] - B[0][1])
	# 			# cos(theta) =  a.b / (||a||*||b||)
	# 			aVec = A[1,:] - A[0,:]
	# 			bVec = B[1,:] - B[0,:]
	# 			cos_sq_theta = np.dot(aVec, bVec)**2
	# 			cos_sq_theta = cos_sq_theta / (np.dot(aVec, aVec))
	# 			cos_sq_theta = cos_sq_theta / (np.dot(bVec, bVec))

	# 			# sumPenalty += (penalties[i][j]/2.0) * (math.cos(theta)**2) * (np.sum(max_zero(-np.matmul(A,u_params[i][j])- gammas[i][j] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[i][j])+ (1+gammas[i][j]) * np.array([1,1]))))
	# 			sumPenalty += (penalties[i][j]/2.0) * (cos_sq_theta) * (np.sum(max_zero(-np.matmul(A,u_params[i][j])- gammas[i][j] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[i][j])+ (1+gammas[i][j]) * np.array([1,1]))))

	# return sumPenalty

	# def parFunc2(i):
	# 	sumPenalty = 0.0
	# 	for j in range(i+1,m):

	# 		# Add the penalty
	# 		# ||(-Au - eY)+||1 + ||(Bu + (1 + Y)e)+||1
	# 		# z_+ = max(0,z)

	# 		# sumPenalty += penalty_ij/2 * [||(-Ai(X)ui - Yie)+||1
	# 		# + ||(Bi(X)ui + (1 + Yi)e)+||1] * cos^2(theta)

	# 		A,B = getEdgePairAsMatrix(X,i,j)
	# 		if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
	# 			# x_pt, y_pt = getIntersection(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
	# 			# theta = getAngleLineSeg(A[0][0], A[0][1], B[0][0], B[0][1], x_pt, y_pt)
				
	# 			# cos(theta) can be computed directly using the dot product between vectors
	# 			# a = (A[1][0] - A[0][0], A[1][1] - A[0][1])
	# 			# b = (B[1][0] - B[0][0], B[1][1] - B[0][1])
	# 			# cos(theta) =  a.b / (||a||*||b||)
	# 			aVec = A[1,:] - A[0,:]
	# 			bVec = B[1,:] - B[0,:]
	# 			cos_sq_theta = np.dot(aVec, bVec)**2
	# 			cos_sq_theta = cos_sq_theta / (np.dot(aVec, aVec))
	# 			cos_sq_theta = cos_sq_theta / (np.dot(bVec, bVec))

	# 			# sumPenalty += (penalties[i][j]/2.0) * (math.cos(theta)**2) * (np.sum(max_zero(-np.matmul(A,u_params[i][j])- gammas[i][j] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[i][j])+ (1+gammas[i][j]) * np.array([1,1]))))
	# 			sumPenalty += (penalties[i][j]/2.0) * (cos_sq_theta) * (np.sum(max_zero(-np.matmul(A,u_params[i][j])- gammas[i][j] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[i][j])+ (1+gammas[i][j]) * np.array([1,1]))))
	# 	return sumPenalty

	partialFunc2 = partial(parFunc2, X)
	pool = Pool(processes = NUM_PROCESSES)
	sumPenalty = pool.map(partialFunc2,range(m))
	pool.close() # no more tasks
	pool.join()  # wrap up current tasks
	sumPenalty = sum(sumPenalty)
	return sumPenalty


def optimize(X_curr):
	# Start with pivotmds or neato stress majorization or cmdscale as in the paper
	# Or use X with a random initialization
	# Currently, we start with neato stress majorization coordinates

	# set penalty to 1 if there is an edge-crossing 
	# reset penalty to 0 if there is no edge-crossing

	X = np.copy(X_curr)

	# loop through all edge pairs
	for i in range(0,m):
		for j in range(i+1,m):
			A,B = getEdgePairAsMatrix(X,i,j)
			if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
				# penalties[i][j] = penalties[i][j] + 1
				penalties[i][j] = 1
			else:
				penalties[i][j] = 0

	#TODO: Be careful that the optimization does not monotonically decrease the cost function
	# This is basically one way to phrase "Unitl Satisfied" and is a very rigid way
	# Another way is to count the number of edge crossings in the graph
	# If the no. of edge crossings remains the same for a long time 
	# or the no. of edge crossings increases significantly
	# or the no. of edge crossings remains within a same range for a long time
	# then stop the optimization and store the embedding with the best edge crossing

	while 1: 

		# For all intersecting edge pairs
		# compute optimal u and gammas using the LP subroutine
		X = np.copy(X_curr)

		# loop through all edge pairs
		for i in range(0,m):
			for j in range(i+1,m):

				A,B = getEdgePairAsMatrix(X,i,j)
				# doIntersect(x11, y11, x12, y12, x21, y21, x22, y22)
				if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
					
					# Call the LP module for optimization
					#Input to the module are two edges A and B
					# A is a 2*2 matrix that contains [a1x a1y; a2x a2y]
					# B is a 2*2 matrix that contains [b1x b1y; b2x b2y]
					# In the LP module, ax = a1x, ay = a1y, bx = a2x, by = a2y
					# cx = b1x, cy = b1y, dx = b2x, dy = b2y
					#u,gamma = LP_optimize(A,B)
					#u_params[i][j] = u
					
					# ux = get_ux(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
					# uy = get_uy(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
					# gamma = get_gamma(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
					
					ux, uy, gamma = get_u_gamma(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])

					u_params[i][j][0] = ux
					u_params[i][j][1] = uy
					gammas[i][j] = gamma
	
		# Use gradient descent to optimize the modified_cost function
		# keep the X as a flattened 1D array and reshape it inside the 
		# modified_cost function as a 2D array/matrix
		X = X.flatten()

		res = minimize(modified_cost, X, method='BFGS', jac=jacobian_Mod_Cost, options={'disp': True})
		X_prev = np.copy(X_curr)
		X_curr = res.x.reshape((n,2))
		print(res.x)

		X = np.copy(X_curr)

		# increase penalty by 1 if the crossing persists 
		# reset penalties to 0 if the crossing disappears

		# loop through all edge pairs
		for i in range(0,m):
			for j in range(i+1,m):
				A,B = getEdgePairAsMatrix(X,i,j)
				if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
					# penalties[i][j] = penalties[i][j] + 1
					penalties[i][j] = 1
				else:
					penalties[i][j] = 0

		if((modified_cost(X_prev) - modified_cost(X_curr)) / modified_cost(X_prev) < EPSILON):
			return X_curr


# Construct the laplacian matrix of the weights
def constructLaplacianMatrix():
	L = -weights
	L[L==-inf] = 0
	diagL = np.diag(np.sum(weights, axis = 1))
	L = L + diagL
	return L

# This function computes the jacobian/gradient of the modified cost function at the point X
def jacobian_Mod_Cost(X):
	#Reshape the 1D array to a n*2 matrix
	global weights
	global distances
	global n
	global W
	global K
	X = X.reshape((n,2))
	dmodCost_dx = np.zeros((n,2))
	dmodCost_dx = dmodCost_dx + W*dStress_dx(X, weights, distances, n)
	dmodCost_dx = dmodCost_dx + K*dSumPenalty_dx(X)
	return dmodCost_dx.flatten()

def parFunc3(X, weights, distances, n, i):
	this_val = np.zeros((1,2))
	# for every node's contribution to this node
	for j in range(0,n):
		diff = X[i,:] - X[j,:]
		# norm_diff = np.linalg.norm(diff)
		norm_diff = np.sqrt(sum(pow(diff,2)))
		if(norm_diff!=0):
			# dStress_dxArr[i,:] += weights[i,j] * (norm_diff - distances[i,j]) * diff / norm_diff
			this_val += weights[i,j] * (norm_diff - distances[i,j]) * diff / norm_diff
	# dStress_dxArr[i,:] *= 2
	this_val *= 2

	return this_val


# This function computes the gradient of stress function at the point X
def dStress_dx(X, weights, distances, n):
	
	dStress_dxArr = np.zeros((n,2))
	
	# for every node/point
	# PAR: parallelizing the outer loop
	# for i in range(0,n):
	# 	# for every node's contribution to this node
	# 	for j in range(0,n):
	# 		diff = X[i,:] - X[j,:]
	# 		# norm_diff = np.linalg.norm(diff)
	# 		norm_diff = np.sqrt(sum(pow(diff,2)))
	# 		if(norm_diff!=0):
	# 			dStress_dxArr[i,:] += weights[i,j] * (norm_diff - distances[i,j]) * diff / norm_diff
	# 	dStress_dxArr[i,:] *= 2

	# def parFunc3(i):
	# 	this_val = np.zeros((1,2))
	# 	# for every node's contribution to this node
	# 	for j in range(0,n):
	# 		diff = X[i,:] - X[j,:]
	# 		# norm_diff = np.linalg.norm(diff)
	# 		norm_diff = np.sqrt(sum(pow(diff,2)))
	# 		if(norm_diff!=0):
	# 			# dStress_dxArr[i,:] += weights[i,j] * (norm_diff - distances[i,j]) * diff / norm_diff
	# 			this_val += weights[i,j] * (norm_diff - distances[i,j]) * diff / norm_diff
	# 	# dStress_dxArr[i,:] *= 2
	# 	this_val *= 2

	# 	return this_val

	partialFunc3 = partial(parFunc3, X, weights, distances, n)
	pool = Pool(processes = NUM_PROCESSES)
	s = pool.map(partialFunc3,range(n))
	pool.close() # no more tasks
	pool.join()  # wrap up current tasks
	for i, item in enumerate(s):
		dStress_dxArr[i,:] = item 
	# print dStress_dxArr
	return dStress_dxArr

def parFunc4(X, this_vertex_crossing):
	# for node_index in vertex_crossing_info:
	node_index = this_vertex_crossing[0]
	edge_pairs = this_vertex_crossing[1]
	this_val = np.zeros((1,2))

	# edge_pairs = vertex_crossing_info[node_index]

	# For each contributing edge pair
	for edge_pairObj in edge_pairs:

		# Get the coordinates of the node
		node_coords = X[node_index, :]

		this_I = edge_pairObj['edge_pair'][0]
		this_J = edge_pairObj['edge_pair'][1]

		this_edge = edge_pairObj['edge']

		A,B = getEdgePairAsMatrix(X,this_I,this_J)

		edgecross_penalty = (np.sum(max_zero(-np.matmul(A,u_params[this_I][this_J])- gammas[this_I][this_J] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[this_I][this_J])+ (1+gammas[this_I][this_J]) * np.array([1,1]))))
		d_edgecross_penalty = np.array([0,0])

		aVec = A[1,:] - A[0,:]
		bVec = B[1,:] - B[0,:]
		cos_sq_theta = np.dot(aVec, bVec)**2
		cos_sq_theta = cos_sq_theta / (np.dot(aVec, aVec))
		cos_sq_theta = cos_sq_theta / (np.dot(bVec, bVec))

		d_cos_sq_theta = np.array([0,0])

		# Retrieve the u_params, gamma and penalty
		# this_ux = u_params[this_I][this_J][0]
		# this_uy = u_params[this_I][this_J][1]
		this_u = u_params[this_I][this_J]
		this_gamma = gammas[this_I][this_J]
		this_penalty = penalties[this_I][this_J]

		# Also include the penalty term in the gradient

		# If the vertex is on left side i.e. associated with -Au - gamma*e
		# if (-Au-gamma*e) > 0, add the derivative -ux, -uy for xi & yi
		# (-xi*ux - yi*uy - gamma > 0)

		if(edge_pairObj['left']):
			if(-np.dot(node_coords, this_u) - this_gamma > 0):
				# dSumPenalty_dxArr[node_index, :] += (-this_u)*this_penalty/2.0
				d_edgecross_penalty = (-this_u)

		# If the vertex is on right side i.e. associated with Bu + (1+gamma)*e
		# if (xi*ux + yi*uy + (1+gamma)) > 0, add the derivative
		# ux, uy for xi & yi
		else:
			if(np.dot(node_coords, this_u) + 1 + this_gamma > 0):
				# dSumPenalty_dxArr[node_index, :] += (this_u)*this_penalty/2.0
				d_edgecross_penalty = (this_u)

		# Note: Add a separate term for crossing angle penalty
		# cos^theta = (a.b)^2 / (||a||^2 * ||b||^2)

		# cos^theta = [(cx - ax)(dx-bx) + (cy-ay)(dy-by)]^2 
		# /[[(cx-ax)^2 + (cy-ay)^2][(dx-bx)^2 + (dy-by)^2]]

		# a = (cx-ax, cy-ay); b = (dx-bx, dy-by)
		
		# d/dcx = d/dcx (constant * f(cx)/ g(cx))
		
		# constant = 1/[(dx-bx)^2 + (dy-by)^2] 
		
		# d/dcx = d/dcx (constant * f(cx) * 1/g(cx))
		
		# = constant * (f(cx)*d/dcx(1/g(cx)) + (1/g(cx)) * d/dcx(f(cx)))
		
		# df(cx)/dcx = 2*sqrt(f(cx))*(dx-bx)
		
		# df(cy)/dcy = 2*sqrt(f(cx))*(dy-by)
		
		# d(1/g(cx))/dcx = - (1 / g(cx)^2) * 2(cx-ax)
		
		# d(1/g(cy))/dcy = - (1 / g(cy)^2) * 2(cy-ay)

		# Start the angle derivative from here
		# if(OPTIMIZE_CROSSING_ANGLE):

		A, B = getEdgePairAsMatrix(X, this_I, this_J)

		if(not(edge_pairObj['left'])):
			A, B = B, A

		temp_ind1, temp_ind2 = getNodesforEdge(this_edge)

		if(temp_ind2 == node_index):
			# Swap the rows of A 
			temp = np.zeros((2,2))
			temp[0,:] = A[1,:]
			temp[1,:] = A[0,:]
			A = temp

		diff_A = A[0,:] - A[1,:]
		diff_B = B[0,:] - B[1,:]

		# fAB = (cx-ax)(dx-bx) + (cy-ay)(dy-by)
		# gAB = 1/[(cx-ax)^2 + (cy-ay)^2]
		# constant = 1/[(dx-bx)^2 + (dy-by)^2]

		# The function cos^2(theta) = constant * fAB^2 * gAB
		# dcos2theta = constant * (fAB^2 * dg_AB + dfsq_AB * gAB)

		fAB = np.dot(diff_A, diff_B)

		dfsq_AB = 2.0 * fAB * diff_B

		gAB = 1.0/((np.dot(diff_A, diff_A)))
		
		# dg_ab = - 2.0 * (1/(sum(np.dot(diff_A, diff_A)))**2) * (diffA)
		dg_ab = -2.0 * (gAB ** 2) * (diff_A) 

		const_term = 1.0 / (np.dot(diff_B, diff_B))

		d_cos_sq_theta = const_term * (fAB**2 * dg_ab + dfsq_AB * gAB)

		## Use the product rule to combine these two derivatives
		# dSumPenalty_dxArr[node_index, :] += (this_penalty/2.0) * (edgecross_penalty*d_cos_sq_theta + d_edgecross_penalty*cos_sq_theta)
		this_val += (this_penalty/2.0) * (edgecross_penalty*d_cos_sq_theta + d_edgecross_penalty*cos_sq_theta)
	return this_val


# This function computes the gradient of sum penalty at the point X
def dSumPenalty_dx(X):

	global u_params
	global gammas
	global penalties
	
	dSumPenalty_dxArr = np.zeros((n,2))

	# Keep a list of all edge pair (indices) that intersect
	list_crossings = []
	# list_crossings.append([5,1])
	# list_crossings = np.array(list_crossings)

	# For each vertex, Keep a list of all edge pairs that intersect where 
	# the vertex is involved
	# Store the edge that intersects
	# Keep track of whether the edge falls to the left/right side of the edge pair
	vertex_crossing_info = {}

	# loop through all edge pairs
	for i in range(0,m):
		for j in range(i+1,m):

			A,B = getEdgePairAsMatrix(X,i,j)
			# doIntersect(x11, y11, x12, y12, x21, y21, x22, y22)
			if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):
				list_crossings.append([i,j])

				# for each vertex appearing in the edge pair
				# store the edge pair, the edge involved, and the left/right side on the edge pair
				i1, i2 = getNodesforEdge(i)
				j1, j2 = getNodesforEdge(j)

				if(not(i1 in vertex_crossing_info)):
					vertex_crossing_info[i1] = []
				if(not(i2 in vertex_crossing_info)):
					vertex_crossing_info[i2] = []
				if(not(j1 in vertex_crossing_info)):
					vertex_crossing_info[j1] = []
				if(not(j2 in vertex_crossing_info)):
					vertex_crossing_info[j2] = []

				i1_obj = {}
				i1_obj['edge_pair'] = [i,j]
				i1_obj['edge'] = i
				i1_obj['left'] = True

				i2_obj = {}
				i2_obj['edge_pair'] = [i,j]
				i2_obj['edge'] = i
				i2_obj['left'] = True

				j1_obj = {}
				j1_obj['edge_pair'] = [i,j]
				j1_obj['edge'] = j
				j1_obj['left'] = False

				j2_obj = {}
				j2_obj['edge_pair'] = [i,j]
				j2_obj['edge'] = j
				j2_obj['left'] = False

				vertex_crossing_info[i1].append(i1_obj)
				vertex_crossing_info[i2].append(i2_obj)
				vertex_crossing_info[j1].append(j1_obj)
				vertex_crossing_info[j2].append(j2_obj)


	# Convert the 2D list into 2D numpy array
	list_crossings = np.array(list_crossings)

	# Loop through the vertex crossings list
	# For each vertex 
	
	# PAR: parallelizing this loop
	# for node_index in vertex_crossing_info:
	# 	edge_pairs = vertex_crossing_info[node_index]

	# 	# For each contributing edge pair
	# 	for edge_pairObj in edge_pairs:

	# 		# Get the coordinates of the node
	# 		node_coords = X[node_index, :]

	# 		this_I = edge_pairObj['edge_pair'][0]
	# 		this_J = edge_pairObj['edge_pair'][1]

	# 		this_edge = edge_pairObj['edge']

	# 		A,B = getEdgePairAsMatrix(X,this_I,this_J)

	# 		edgecross_penalty = (np.sum(max_zero(-np.matmul(A,u_params[this_I][this_J])- gammas[this_I][this_J] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[this_I][this_J])+ (1+gammas[this_I][this_J]) * np.array([1,1]))))
	# 		d_edgecross_penalty = np.array([0,0])
	
	# 		aVec = A[1,:] - A[0,:]
	# 		bVec = B[1,:] - B[0,:]
	# 		cos_sq_theta = np.dot(aVec, bVec)**2
	# 		cos_sq_theta = cos_sq_theta / (np.dot(aVec, aVec))
	# 		cos_sq_theta = cos_sq_theta / (np.dot(bVec, bVec))

	# 		d_cos_sq_theta = np.array([0,0])

	# 		# Retrieve the u_params, gamma and penalty
	# 		# this_ux = u_params[this_I][this_J][0]
	# 		# this_uy = u_params[this_I][this_J][1]
	# 		this_u = u_params[this_I][this_J]
	# 		this_gamma = gammas[this_I][this_J]
	# 		this_penalty = penalties[this_I][this_J]

	# 		# Also include the penalty term in the gradient

	# 		# If the vertex is on left side i.e. associated with -Au - gamma*e
	# 		# if (-Au-gamma*e) > 0, add the derivative -ux, -uy for xi & yi
	# 		# (-xi*ux - yi*uy - gamma > 0)

	# 		if(edge_pairObj['left']):
	# 			if(-np.dot(node_coords, this_u) - this_gamma > 0):
	# 				# dSumPenalty_dxArr[node_index, :] += (-this_u)*this_penalty/2.0
	# 				d_edgecross_penalty = (-this_u)

	# 		# If the vertex is on right side i.e. associated with Bu + (1+gamma)*e
	# 		# if (xi*ux + yi*uy + (1+gamma)) > 0, add the derivative
	# 		# ux, uy for xi & yi
	# 		else:
	# 			if(np.dot(node_coords, this_u) + 1 + this_gamma > 0):
	# 				# dSumPenalty_dxArr[node_index, :] += (this_u)*this_penalty/2.0
	# 				d_edgecross_penalty = (this_u)

	# 		# Note: Add a separate term for crossing angle penalty
	# 		# cos^theta = (a.b)^2 / (||a||^2 * ||b||^2)

	# 		# cos^theta = [(cx - ax)(dx-bx) + (cy-ay)(dy-by)]^2 
	# 		# /[[(cx-ax)^2 + (cy-ay)^2][(dx-bx)^2 + (dy-by)^2]]

	# 		# a = (cx-ax, cy-ay); b = (dx-bx, dy-by)
			
	# 		# d/dcx = d/dcx (constant * f(cx)/ g(cx))
			
	# 		# constant = 1/[(dx-bx)^2 + (dy-by)^2] 
			
	# 		# d/dcx = d/dcx (constant * f(cx) * 1/g(cx))
			
	# 		# = constant * (f(cx)*d/dcx(1/g(cx)) + (1/g(cx)) * d/dcx(f(cx)))
			
	# 		# df(cx)/dcx = 2*sqrt(f(cx))*(dx-bx)
			
	# 		# df(cy)/dcy = 2*sqrt(f(cx))*(dy-by)
			
	# 		# d(1/g(cx))/dcx = - (1 / g(cx)^2) * 2(cx-ax)
			
	# 		# d(1/g(cy))/dcy = - (1 / g(cy)^2) * 2(cy-ay)

	# 		# Start the angle derivative from here
	# 		# if(OPTIMIZE_CROSSING_ANGLE):

	# 		A, B = getEdgePairAsMatrix(X, this_I, this_J)

	# 		if(not(edge_pairObj['left'])):
	# 			A, B = B, A

	# 		temp_ind1, temp_ind2 = getNodesforEdge(this_edge)

	# 		if(temp_ind2 == node_index):
	# 			# Swap the rows of A 
	# 			temp = np.zeros((2,2))
	# 			temp[0,:] = A[1,:]
	# 			temp[1,:] = A[0,:]
	# 			A = temp

	# 		diff_A = A[0,:] - A[1,:]
	# 		diff_B = B[0,:] - B[1,:]

	# 		# fAB = (cx-ax)(dx-bx) + (cy-ay)(dy-by)
	# 		# gAB = 1/[(cx-ax)^2 + (cy-ay)^2]
	# 		# constant = 1/[(dx-bx)^2 + (dy-by)^2]

	# 		# The function cos^2(theta) = constant * fAB^2 * gAB
	# 		# dcos2theta = constant * (fAB^2 * dg_AB + dfsq_AB * gAB)

	# 		fAB = np.dot(diff_A, diff_B)

	# 		dfsq_AB = 2.0 * fAB * diff_B

	# 		gAB = 1.0/((np.dot(diff_A, diff_A)))
			
	# 		# dg_ab = - 2.0 * (1/(sum(np.dot(diff_A, diff_A)))**2) * (diffA)
	# 		dg_ab = -2.0 * (gAB ** 2) * (diff_A) 

	# 		const_term = 1.0 / (np.dot(diff_B, diff_B))

	# 		d_cos_sq_theta = const_term * (fAB**2 * dg_ab + dfsq_AB * gAB)

	# 		## Use the product rule to combine these two derivatives
	# 		dSumPenalty_dxArr[node_index, :] += (this_penalty/2.0) * (edgecross_penalty*d_cos_sq_theta + d_edgecross_penalty*cos_sq_theta)

	# return dSumPenalty_dxArr

	# def parFunc4(this_vertex_crossing):
	# # for node_index in vertex_crossing_info:
	# 	node_index = this_vertex_crossing[0]
	# 	edge_pairs = this_vertex_crossing[1]
	# 	this_val = np.zeros((1,2))

	# 	# edge_pairs = vertex_crossing_info[node_index]

	# 	# For each contributing edge pair
	# 	for edge_pairObj in edge_pairs:

	# 		# Get the coordinates of the node
	# 		node_coords = X[node_index, :]

	# 		this_I = edge_pairObj['edge_pair'][0]
	# 		this_J = edge_pairObj['edge_pair'][1]

	# 		this_edge = edge_pairObj['edge']

	# 		A,B = getEdgePairAsMatrix(X,this_I,this_J)

	# 		edgecross_penalty = (np.sum(max_zero(-np.matmul(A,u_params[this_I][this_J])- gammas[this_I][this_J] * np.array([1,1]))) + np.sum(max_zero(np.matmul(B,u_params[this_I][this_J])+ (1+gammas[this_I][this_J]) * np.array([1,1]))))
	# 		d_edgecross_penalty = np.array([0,0])
	
	# 		aVec = A[1,:] - A[0,:]
	# 		bVec = B[1,:] - B[0,:]
	# 		cos_sq_theta = np.dot(aVec, bVec)**2
	# 		cos_sq_theta = cos_sq_theta / (np.dot(aVec, aVec))
	# 		cos_sq_theta = cos_sq_theta / (np.dot(bVec, bVec))

	# 		d_cos_sq_theta = np.array([0,0])

	# 		# Retrieve the u_params, gamma and penalty
	# 		# this_ux = u_params[this_I][this_J][0]
	# 		# this_uy = u_params[this_I][this_J][1]
	# 		this_u = u_params[this_I][this_J]
	# 		this_gamma = gammas[this_I][this_J]
	# 		this_penalty = penalties[this_I][this_J]

	# 		# Also include the penalty term in the gradient

	# 		# If the vertex is on left side i.e. associated with -Au - gamma*e
	# 		# if (-Au-gamma*e) > 0, add the derivative -ux, -uy for xi & yi
	# 		# (-xi*ux - yi*uy - gamma > 0)

	# 		if(edge_pairObj['left']):
	# 			if(-np.dot(node_coords, this_u) - this_gamma > 0):
	# 				# dSumPenalty_dxArr[node_index, :] += (-this_u)*this_penalty/2.0
	# 				d_edgecross_penalty = (-this_u)

	# 		# If the vertex is on right side i.e. associated with Bu + (1+gamma)*e
	# 		# if (xi*ux + yi*uy + (1+gamma)) > 0, add the derivative
	# 		# ux, uy for xi & yi
	# 		else:
	# 			if(np.dot(node_coords, this_u) + 1 + this_gamma > 0):
	# 				# dSumPenalty_dxArr[node_index, :] += (this_u)*this_penalty/2.0
	# 				d_edgecross_penalty = (this_u)

	# 		# Note: Add a separate term for crossing angle penalty
	# 		# cos^theta = (a.b)^2 / (||a||^2 * ||b||^2)

	# 		# cos^theta = [(cx - ax)(dx-bx) + (cy-ay)(dy-by)]^2 
	# 		# /[[(cx-ax)^2 + (cy-ay)^2][(dx-bx)^2 + (dy-by)^2]]

	# 		# a = (cx-ax, cy-ay); b = (dx-bx, dy-by)
			
	# 		# d/dcx = d/dcx (constant * f(cx)/ g(cx))
			
	# 		# constant = 1/[(dx-bx)^2 + (dy-by)^2] 
			
	# 		# d/dcx = d/dcx (constant * f(cx) * 1/g(cx))
			
	# 		# = constant * (f(cx)*d/dcx(1/g(cx)) + (1/g(cx)) * d/dcx(f(cx)))
			
	# 		# df(cx)/dcx = 2*sqrt(f(cx))*(dx-bx)
			
	# 		# df(cy)/dcy = 2*sqrt(f(cx))*(dy-by)
			
	# 		# d(1/g(cx))/dcx = - (1 / g(cx)^2) * 2(cx-ax)
			
	# 		# d(1/g(cy))/dcy = - (1 / g(cy)^2) * 2(cy-ay)

	# 		# Start the angle derivative from here
	# 		# if(OPTIMIZE_CROSSING_ANGLE):

	# 		A, B = getEdgePairAsMatrix(X, this_I, this_J)

	# 		if(not(edge_pairObj['left'])):
	# 			A, B = B, A

	# 		temp_ind1, temp_ind2 = getNodesforEdge(this_edge)

	# 		if(temp_ind2 == node_index):
	# 			# Swap the rows of A 
	# 			temp = np.zeros((2,2))
	# 			temp[0,:] = A[1,:]
	# 			temp[1,:] = A[0,:]
	# 			A = temp

	# 		diff_A = A[0,:] - A[1,:]
	# 		diff_B = B[0,:] - B[1,:]

	# 		# fAB = (cx-ax)(dx-bx) + (cy-ay)(dy-by)
	# 		# gAB = 1/[(cx-ax)^2 + (cy-ay)^2]
	# 		# constant = 1/[(dx-bx)^2 + (dy-by)^2]

	# 		# The function cos^2(theta) = constant * fAB^2 * gAB
	# 		# dcos2theta = constant * (fAB^2 * dg_AB + dfsq_AB * gAB)

	# 		fAB = np.dot(diff_A, diff_B)

	# 		dfsq_AB = 2.0 * fAB * diff_B

	# 		gAB = 1.0/((np.dot(diff_A, diff_A)))
			
	# 		# dg_ab = - 2.0 * (1/(sum(np.dot(diff_A, diff_A)))**2) * (diffA)
	# 		dg_ab = -2.0 * (gAB ** 2) * (diff_A) 

	# 		const_term = 1.0 / (np.dot(diff_B, diff_B))

	# 		d_cos_sq_theta = const_term * (fAB**2 * dg_ab + dfsq_AB * gAB)

	# 		## Use the product rule to combine these two derivatives
	# 		# dSumPenalty_dxArr[node_index, :] += (this_penalty/2.0) * (edgecross_penalty*d_cos_sq_theta + d_edgecross_penalty*cos_sq_theta)
	# 		this_val += (this_penalty/2.0) * (edgecross_penalty*d_cos_sq_theta + d_edgecross_penalty*cos_sq_theta)
	# 	return this_val

	partialFunc4 = partial(parFunc4, X)
	pool = Pool(processes = NUM_PROCESSES)
	vertex_crossing_list = vertex_crossing_info.items()
	s = pool.map(partialFunc4, vertex_crossing_list)
	pool.close() # no more tasks
	pool.join()  # wrap up current tasks
	for i, item in enumerate(s):
		dSumPenalty_dxArr[vertex_crossing_list[i][0],:] = item 
	# print dSumPenalty_dxArr
	
	return dSumPenalty_dxArr

	
if __name__ == '__main__':
	plotGraphandStats(X_curr)
	X_curr = optimize(X_curr)
	plotGraphandStats(X_curr)













