import matplotlib.pyplot as plt
from iterators import *
from input_functions import *
from edge_crossing import *

my_inf = 10000000

def find_min_angle(G, X):
 edge_list = G.edges()
 m = len(edge_list)
 # for every edge in the graph
 # draw a line joining the endpoints
 for i in range(0,m):
  i1, i2 = getNodesforEdge(i, edge_list)
  A = np.zeros((2,2))

  A[0,:] = X[i1, :]
  A[1,:] = X[i2, :]
  plt.plot(A[:,0] , A[:,1], color='blue')

 num_intersections = 0
 min_angle = math.pi/2.0

 print("Number of edges: ", m)

 # loop through all edge pairs
 for i in range(0,m):
  for j in range(i+1,m):

   A,B = getEdgePairAsMatrix(X,i,j,edge_list)
   # doIntersect(x11, y11, x12, y12, x21, y21, x22, y22)
   if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):

    # print A
    # print B
    # print i
    # print j
    x_pt, y_pt = getIntersection(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
    theta = getAngleLineSeg(A[0][0], A[0][1], B[0][0], B[0][1], x_pt, y_pt)
    if theta > math.pi/2.0:
     theta = math.pi - theta
    if theta < min_angle:
     min_angle = theta

    num_intersections += 1

 print("Number of Edge Crossings: ", num_intersections)
 print("Minimum Angle: ", to_deg(min_angle))
 return to_deg(min_angle)

#This function returns the nodes of an edge given its index in the edge list
def getNodesforEdge(index, edge_list):
        #print(index)
        #print(edge_list)
        #print('type(edge_list)')
        #print(type(edge_list))
        #print('type(edge_list[index])')
        #print(type(edge_list[index]))
        #print(str(edge_list[index]))
        #print(str(edge_list[index][0]))
        return edge_list[index][0], edge_list[index][1]


# This function extracts the edge pair in the form of matrices
# Returns two matrices A and B
# A contains [a1x, a1y; a2x a2y]
# B contains [b1x, b1y; b2x b2y]
def getEdgePairAsMatrix(X,i,j,edge_list):
        A = np.zeros((2,2))
        B = np.zeros((2,2))

        i1, i2 = getNodesforEdge(i,edge_list)
        j1, j2 = getNodesforEdge(j,edge_list)

        A[0,:] = X[i1, :]
        A[1,:] = X[i2, :]

        B[0,:] = X[j1, :]
        B[1,:] = X[j2, :]

        return A,B


def plot_angle(folder_name, part_of_file_name, arr_xy, plot_suffix):
 global my_inf
 overall_min_crossing = my_inf
 minW = 0
 minK = 0
 minIter = 0
 neato_wins = 0
 G = build_networkx_graph(folder_name + part_of_file_name + '.txt')
 nW = len(arr_xy)
 #nK = len(arr_xy[0])
 #print(type(arr_xy))
 #print(type(arr_xy[0]))
 #print(type(arr_xy[0][0]))
 #print(type(arr_xy[0][0][0]))
 #nIter, nodes, dim = arr_xy[0][0].shape
 nK, nIter, nodes, dim = arr_xy[0].shape
 fig_number = 0
 for w in range(nW):
  caption = 'W='+str(w)+', ' + part_of_file_name
  random_min_angle = []
  random_max_angle = []
  neato_min_angle = []
  neato_max_angle = []
  for k in range(nK):
   random_min_angle.append(180)
   random_max_angle.append(-1)
   neato_min_angle.append(180)
   neato_max_angle.append(-1)
   for i in range(nIter):
    if i<(nIter/2):
     if find_min_angle(G, arr_xy[w][k][i]) < neato_min_angle[k]:
      #neato_min_crossings[k] = arr_ncr[w][k][i]
      # crossing should not be fraction, but for input 1 it is fraction!!!!
      # temporary fix, cast to int
      neato_min_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
      if overall_min_crossing > neato_min_angle[k]:
       overall_min_crossing = neato_min_angle[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 1
     if find_min_angle(G, arr_xy[w][k][i]) > neato_max_angle[k]:
      #neato_max_crossings[k] = arr_ncr[w][k][i]
      neato_max_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
    else:
     if find_min_angle(G, arr_xy[w][k][i]) < random_min_angle[k]:
      #random_min_crossings[k] = arr_ncr[w][k][i]
      random_min_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
      if overall_min_crossing > random_min_angle[k]:
       overall_min_crossing = random_min_angle[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 0
     if find_min_angle(G, arr_xy[w][k][i]) > random_max_angle[k]:
      #random_max_crossings[k] = arr_ncr[w][k][i]
      random_max_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
     #print('i:'+str(i)+',crossing:'+str(arr_ncr[w][k][i])+',min:'+str(random_min_crossings[k])+',max:'+str(random_max_crossings[k]))
  fig = plt.figure(fig_number)
  fig_number = fig_number + 1
  plt.plot(range(-int(nK/2),int(nK/2)), random_min_angle, 'r--', label='random min')
  plt.plot(range(-int(nK/2),int(nK/2)), random_max_angle, 'go', label='random max')
  plt.plot(range(-int(nK/2),int(nK/2)), neato_min_angle, 'bs', label='neato min')
  plt.plot(range(-int(nK/2),int(nK/2)), neato_max_angle, 'y^', label='neato max')
  plt.legend(loc=1)
  plt.title(caption)
  plt.ylabel('Crossing angle')
  plt.xlabel('log(K)')
  plt.savefig(folder_name+part_of_file_name+'_angle_'+str(w)+plot_suffix+'.png')
  plt.close(fig)
 #print('overall_min_crossing: '+str(overall_min_crossing))
 return minW, minK, minIter, neato_wins, overall_min_crossing


NUMBER_OF_CROSSINGS, COST_FUNCTIONS, arr_xy, init_X = jacob_iter('../graphs/input', jacob_read)
for i in range(len(arr_xy)):
 for j in range(len(arr_xy[0])):
  plot_angle('../graphs/input/', 'input'+str(j+1), arr_xy[i][j], '_NORM_'+str(i))


