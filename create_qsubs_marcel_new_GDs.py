import math
from iterators import *
from input_functions import *

def create_qsub_er_angle():
 #folder_name = "meeting_08_21_2018/todo2"
 folder_name = "profile_smart_algorithm"

 #file_names = ["er_8_0.6", "er_10_0.6", "er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_5_0.6", "er_6_0.6", "er_7_0.6"]
 file_names = ["er_50_0.1", "er_50_0.2", "er_100_0.1", "er_100_0.2", "er_150_0.1", "er_150_0.2", "er_200_0.1", "er_200_0.2"]

 f = open('qsub_commands_angle.sh','w')
 #for n in range(2):
 for n in range(1):
  for i in range(len(file_names)):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX='+file_names[i]+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files_angle/edge_crossings'+file_names[i]+'.out -e log_files_angle/edge_crossings'+file_names[i]+'.err -V edge_crossings_angle.sh\n')
 f.close()

def create_qsub_er():
 #folder_name = "meeting_08_21_2018/todo2"
 folder_name = "profile_smart_algorithm"

 #file_names = ["er_8_0.6", "er_10_0.6", "er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_5_0.6", "er_6_0.6", "er_7_0.6"]
 file_names = ["er_50_0.1", "er_50_0.2", "er_100_0.1", "er_100_0.2", "er_150_0.1", "er_150_0.2", "er_200_0.1", "er_200_0.2"]

 f = open('qsub_commands.sh','w')
 #for n in range(2):
 for n in range(1):
  for i in range(len(file_names)):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX='+file_names[i]+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files/edge_crossings'+file_names[i]+'.out -e log_files/edge_crossings'+file_names[i]+'.err -V edge_crossings.sh\n')
 f.close()

def create_qsub_input():
 folder_name = "input"
 f = open('qsub_commands.sh','w')
 for n in range(2):
  for i in range(1,6):
  #for i in range(2,6):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX=input'+str(i)+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files_init_xy/edge_crossings_input'+str(i)+'_'+str(n)+'.out -e log_files_init_xy/edge_crossings_input'+str(i)+'_'+str(n)+'.err -V edge_crossings.sh\n')
 f.close()

#create_qsub_input()

def create_qsub_input_angle():
 folder_name = "input_angle"
 f = open('qsub_commands_angle.sh','w')
 for n in range(2):
  for i in range(1,6):
  #for i in range(2,6):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX=input'+str(i)+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files_angle/edge_crossings_input'+str(i)+'_'+str(n)+'.out -e log_files_angle/edge_crossings_input'+str(i)+'_'+str(n)+'.err -V edge_crossings_angle.sh\n')
 f.close()


def create_qsub_jacob():
 f = open('qsub_commands_jacob.sh','w')
 f.close()

 #folder_name = "meeting_08_21_2018/todo2"
 #folder_name = "profile_smart_algorithm"
 folder_name = "input_angle"

 jacob_iter(folder_name, jacob_write)

def create_qsub_jacob_angle():
 f = open('qsub_commands_jacob.sh','w')
 f.close()

 #folder_name = "meeting_08_21_2018/todo2"
 #folder_name = "profile_smart_algorithm"
 folder_name = "input_angle"

 jacob_iter(folder_name, jacob_write_angle)

def jacob_write_angle(folder_name, file_names, i, W, K_start, K_end, n, NUM_RUNS, NUM_ITERATIONS, job_name_prefix, log_folder, pbs_script_name):
 f = open('qsub_commands_marcel.sh','a')
 f.write('export OUTPUT_FOLDER='+folder_name+'\n')
 f.write('export FILE_NAME_PREFIX='+file_names[i]+'\n')
 f.write('export W_start='+str(W)+'\n')
 f.write('export W_end='+str(W+1)+'\n')
 f.write('export K_start='+str(K_start)+'\n')
 f.write('export K_end='+str(K_end)+'\n')
 f.write('export NUM_RUNS='+str(NUM_RUNS)+'\n')
 f.write('export num_iterations='+str(NUM_ITERATIONS)+'\n')
 f.write('export NORMALIZE='+str(n)+'\n')
 K_start_str = str(K_start)
 if K_start<0:
  K_start_str = 'N'+str(-K_start)
 K_end_str = str(K_end)
 if K_end<0:
  K_end_str = 'N'+str(-K_end)
 f.write('export OUTPUT_FILE_EXT='+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'\n')
 #f.write('./edge_crossings_param_angle.sh&\n')
 f.write('qsub -N ' + job_name_prefix+'_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str + ' -o ' + log_folder  + '/' + job_name_prefix + '_'+file_names[i]+'_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.out -e ' + log_folder  + '/' + job_name_prefix + '_'+file_names[i]+'_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.err -V ' + pbs_script_name + '\n')
 f.close()
 return [0, 0, 0, 0]

import os

def create_qsub_variable_param(edge_threshold, folder_name, job_name_prefix, log_folder, pbs_script_name):
 f = open('qsub_commands_marcel.sh','w')
 f.close()

 file_names = []
 #for name in os.listdir(folder_name):
 # if name=='.gml.txt':
 #  continue
 # if name[len(name)-4:len(name)]=='.txt':
 #  file_names.append(name[:len(name)-4])
 #file_names = ["grafo1233.70", "grafo3836.53", "grafo4430.72", "grafo4509.64", "grafo4680.76", "grafo7021.71", "grafo7801.70", "grafo8053.62", "grafo8386.73", "grafo8390.69", "grafo8588.71", "grafo9308.63", "grafo9673.64", "grafo9796.73"]
 #file_names = ["g.35.32", "g.36.17", "g.53.9", "g.56.10", "g.58.0", "g.61.3", "g.63.1", "g.64.17", "g.66.8", "g.75.0", "g.75.4", "g.85.8"]
 file_names = ["g.61.3"]


 W_start = 1
 W_end = 2

 #K_start = -5
 #K_end = 6

 K_start = -3
 K_end = 3

 NUM_RUNS = 6
 NUM_ITERATIONS = 3
 #job_name_prefix = 'edge_crossings'

 #for n in range(2):
 for n in range(1):
  for W in range(W_start, W_end):
   for i in range(len(file_names)):
    dummy, coord_list, edge_list = take_input(folder_name + '/' + file_names[i]+'.txt')
    if len(edge_list)>edge_threshold:
     NUM_RUNS = 2
     NUM_ITERATIONS = 2
     for K in range(K_start,K_end):
      res_arr = jacob_write_angle(folder_name, file_names, i, W, K, K+1, n, NUM_RUNS, NUM_ITERATIONS, job_name_prefix, log_folder, pbs_script_name)
    else:
     NUM_RUNS = 6
     NUM_ITERATIONS = 3
     res_arr = jacob_write_angle(folder_name, file_names, i, W, K_start, K_end, n, NUM_RUNS, NUM_ITERATIONS, job_name_prefix, log_folder, pbs_script_name)

 #return NUMBER_OF_CROSSINGS, COST_FUNCTIONS, X_new, init_X



#create_qsub_input_angle()
#create_qsub_er()
#create_qsub_er_angle()
#create_qsub_jacob()
#create_qsub_jacob_angle()
#create_qsub_variable_param(14, 100)
#create_qsub_variable_param(9, 14, 100) #start from input 10

#create_qsub_variable_param(100, "crossing_min_random_drawing/community", 'edge_crossings', 'log_files_community', 'edge_crossings_marcel.sh')
create_qsub_variable_param(100, "crossing_min_random_drawing/north", 'edge_crossings', 'log_files_north', 'edge_crossings_marcel.sh')
#create_qsub_variable_param(100, "crossing_min_random_drawing/plantri", 'edge_crossings', 'log_files_plantri', 'edge_crossings_marcel.sh')
#create_qsub_variable_param(100, "crossing_min_random_drawing/rome", 'edge_crossings', 'log_files_rome', 'edge_crossings_marcel.sh')

