import sys
import json
if len(sys.argv) < 2:
	print('Usage: python3 phase1.py input_file.txt')
	quit()

input_file = sys.argv[1]
#graph_property = 'weighted'
graph_property = 'unweighted'
file = open(input_file,"r")
print("File name: "+input_file)
graph = json.loads(file.read())
print(graph)

n = len(graph['nodes'])
matrix = [[0] * n for i in range(n)]

for e in graph['edges']:
        matrix[e['target']][e['source']] = matrix[e['source']][e['target']] = 1

print(matrix)

