import math
from iterators import *
from input_functions import *

def create_qsub_er_angle():
 #folder_name = "meeting_08_21_2018/todo2"
 folder_name = "profile_smart_algorithm"

 #file_names = ["er_8_0.6", "er_10_0.6", "er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_5_0.6", "er_6_0.6", "er_7_0.6"]
 file_names = ["er_50_0.1", "er_50_0.2", "er_100_0.1", "er_100_0.2", "er_150_0.1", "er_150_0.2", "er_200_0.1", "er_200_0.2"]

 f = open('qsub_commands_angle.sh','w')
 #for n in range(2):
 for n in range(1):
  for i in range(len(file_names)):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX='+file_names[i]+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files_angle/edge_crossings'+file_names[i]+'.out -e log_files_angle/edge_crossings'+file_names[i]+'.err -V edge_crossings_angle.sh\n')
 f.close()

def create_qsub_er():
 #folder_name = "meeting_08_21_2018/todo2"
 folder_name = "profile_smart_algorithm"

 #file_names = ["er_8_0.6", "er_10_0.6", "er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_5_0.6", "er_6_0.6", "er_7_0.6"]
 file_names = ["er_50_0.1", "er_50_0.2", "er_100_0.1", "er_100_0.2", "er_150_0.1", "er_150_0.2", "er_200_0.1", "er_200_0.2"]

 f = open('qsub_commands.sh','w')
 #for n in range(2):
 for n in range(1):
  for i in range(len(file_names)):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX='+file_names[i]+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files/edge_crossings'+file_names[i]+'.out -e log_files/edge_crossings'+file_names[i]+'.err -V edge_crossings.sh\n')
 f.close()

def create_qsub_input():
 folder_name = "input"
 f = open('qsub_commands.sh','w')
 for n in range(2):
  for i in range(1,6):
  #for i in range(2,6):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX=input'+str(i)+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files_init_xy/edge_crossings_input'+str(i)+'_'+str(n)+'.out -e log_files_init_xy/edge_crossings_input'+str(i)+'_'+str(n)+'.err -V edge_crossings.sh\n')
 f.close()

#create_qsub_input()

def create_qsub_input_angle():
 folder_name = "input_angle"
 f = open('qsub_commands_angle.sh','w')
 for n in range(2):
  for i in range(1,6):
  #for i in range(2,6):
   f.write('export NORMALIZE='+str(n)+'\n')
   f.write('export FILE_NAME_PREFIX=input'+str(i)+'\n')
   f.write('export OUTPUT_FOLDER='+folder_name+'\n')
   f.write('qsub -N edge_crossings -o log_files_angle/edge_crossings_input'+str(i)+'_'+str(n)+'.out -e log_files_angle/edge_crossings_input'+str(i)+'_'+str(n)+'.err -V edge_crossings_angle.sh\n')
 f.close()


def create_qsub_jacob():
 f = open('qsub_commands_jacob.sh','w')
 f.close()

 #folder_name = "meeting_08_21_2018/todo2"
 #folder_name = "profile_smart_algorithm"
 folder_name = "input_angle"

 jacob_iter(folder_name, jacob_write)

def create_qsub_jacob_angle():
 f = open('qsub_commands_jacob.sh','w')
 f.close()

 #folder_name = "meeting_08_21_2018/todo2"
 #folder_name = "profile_smart_algorithm"
 folder_name = "input_angle"

 jacob_iter(folder_name, jacob_write_angle)

def jacob_write_angle(folder_name, file_names, i, W, K_start, K_end, n, layout_file, NUM_ITERATIONS, job_name_prefix, log_folder, pbs_script_name, strategy, alpha, GRADIENT_DESCENT_ITERATIONS, layout_ext):
 f = open('qsub_commands_new_GDs.sh','a')
 f.write('export OUTPUT_FOLDER='+folder_name+'\n')
 f.write('export FILE_NAME_PREFIX='+file_names[i]+'\n')
 f.write('export W_start='+str(W)+'\n')
 f.write('export W_end='+str(W+1)+'\n')
 f.write('export K_start='+str(K_start)+'\n')
 f.write('export K_end='+str(K_end)+'\n')
 f.write('export USE_INITIAL='+layout_file+'\n')
 f.write('export num_iterations='+str(NUM_ITERATIONS)+'\n')
 f.write('export NORMALIZE='+str(n)+'\n')
 f.write('export STRATEGY='+strategy+'\n')
 f.write('export ALPHA='+alpha+'\n')
 f.write('export GRADIENT_DESCENT_ITERATIONS='+str(GRADIENT_DESCENT_ITERATIONS)+'\n')
 K_start_str = str(K_start)
 if K_start<0:
  K_start_str = 'N'+str(-K_start)
 K_end_str = str(K_end)
 if K_end<0:
  K_end_str = 'N'+str(-K_end)
 file_ext = 'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'_STRAT_'+strategy+'_'+layout_ext
 f.write('export OUTPUT_FILE_EXT='+file_ext+'\n')
 #f.write('./edge_crossings_param_angle.sh&\n')
 f.write('qsub -N ' + job_name_prefix+'_'+ file_ext + ' -o ' + log_folder  + '/' + job_name_prefix + '_'+file_names[i]+'_'+ file_ext +'.out -e ' + log_folder  + '/' + job_name_prefix + '_'+file_names[i]+'_'+ file_ext +'.err -V ' + pbs_script_name + '\n')
 f.close()
 return [0, 0, 0, 0]


def create_qsub_variable_param(number_of_inputs_start, number_of_inputs_end, edge_threshold, folder_name, job_name_prefix, log_folder, pbs_script_name_small_graph, pbs_script_name_large_graph, iterations_small_graph, iterations_large_graph, gd_iter_small_graph, gd_iter_large_graph):
 f = open('qsub_commands_new_GDs.sh','w')
 f.close()

 #folder_name = "input_angle"
 #folder_name = "../graphs/input18"
 file_names = []
 for i in range(number_of_inputs_start, number_of_inputs_end):
  file_names.append("input"+str(i+1))
 #file_names = ["input1", "input2", "input3", "input4", "input5", "input6"]

 W_start = 1
 W_end = 2

 #K_start = -5
 #K_end = 6

 K_start = -5
 K_end = 5

 NUM_RUNS = 15
 NUM_ITERATIONS = iterations_small_graph
 GRADIENT_DESCENT_ITERATIONS = gd_iter_small_graph
 #job_name_prefix = 'edge_crossings'
 GD_OPTIONS = ['VANILLA', 'MOMENTUM', 'NESTEROV', 'ADAGRAD', 'RMSPROP', 'ADAM']
 alpha_arr = ['1e-3', '1e-3', '1e-3', '1e-1', '1e-1', '1e-1'] # learning rate

 pbs_script_name = pbs_script_name_small_graph

 #for n in range(2):
 for n in range(1):
  for W in range(W_start, W_end):
   for i in range(len(file_names)):
    dummy, coord_list, edge_list = take_input(folder_name + '/' + file_names[i]+'.txt')
    for s in range(len(GD_OPTIONS)):
     alpha = alpha_arr[s]
     layout_files = []
     if len(edge_list)>edge_threshold:
      NUM_ITERATIONS = iterations_large_graph
      GRADIENT_DESCENT_ITERATIONS = gd_iter_large_graph
      alpha = '1e-1'
      K_start = 1
      K_end = 2
      layout = ['neato', 'sfdp', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '0', '0']
      for l in range(len(layout)):
       if layout[l]=='random':
        layout_files.append('0')
       else:
        layout_files.append('run_'+layout[l]+'_'+file_names[i]+'_'+layout_number[l]+'.dot')
      pbs_script_name = pbs_script_name_large_graph
      #for K in range(K_start,K_end):
      # res_arr = jacob_write_angle(folder_name, file_names, i, W, K, K+1, n, NUM_RUNS, NUM_ITERATIONS, job_name_prefix, log_folder, pbs_script_name)
     else:
      NUM_ITERATIONS = iterations_small_graph
      GRADIENT_DESCENT_ITERATIONS = gd_iter_small_graph
      K_start = -5
      K_end = 5
      layout = ['neato', 'neato', 'neato', 'neato', 'neato', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'sfdp', 'random', 'random', 'random', 'random', 'random']
      NUM_RUNS = len(layout)
      layout_number = ['0', '1', '2', '3', '4', '0', '1', '2', '3', '4', '0', '1', '2', '3', '4']
      for l in range(len(layout)):
       if layout[l]=='random':
        layout_files.append('0')
       else:
        layout_files.append('run_'+layout[l]+'_'+file_names[i]+'_'+layout_number[l]+'.dot')
      pbs_script_name = pbs_script_name_small_graph
      #res_arr = jacob_write_angle(folder_name, file_names, i, W, K_start, K_end, n, NUM_RUNS, NUM_ITERATIONS, job_name_prefix, log_folder, pbs_script_name)
     for r in range(0,NUM_RUNS):
      res_arr = jacob_write_angle(folder_name, file_names, i, W, K_start, K_end, n, layout_files[r], NUM_ITERATIONS, job_name_prefix, log_folder, pbs_script_name, GD_OPTIONS[s], alpha, GRADIENT_DESCENT_ITERATIONS, layout[r]+'_'+layout_number[r])
 #return NUMBER_OF_CROSSINGS, COST_FUNCTIONS, X_new, init_X



#create_qsub_input_angle()
#create_qsub_er()
#create_qsub_er_angle()
#create_qsub_jacob()
#create_qsub_jacob_angle()
#create_qsub_variable_param(14, 100)
#create_qsub_variable_param(9, 14, 100) #start from input 10

#create_qsub_variable_param(11, 14, 100, "input_angle", 'crossings_angle', 'log_files_angle', 'crossings_angle_high_pri.sh') #start from input 1 for 2018
#create_qsub_variable_param(11, 14, 100, "input_crossing", 'edge_crossings', 'log_files_crossing', 'edge_crossings_high_pri.sh') #start from input 1 for 2018
#create_qsub_variable_param(0, 16, 100, "input_angle_17", 'crossings_angle', 'log_files_angle_17', 'crossings_angle_high_pri.sh') #start from input 1 for 2017
#create_qsub_variable_param(0, 16, 100, "input_crossing_17", 'edge_crossings', 'log_files_crossing_17', 'edge_crossings_high_pri.sh') #start from input 1 for 2017

#create_qsub_variable_param(7, 14, 100, "input_partial_func", 'cross_anlge_par', 'log_files_angle_par', 'edge_crossings_par.sh')

#create_qsub_variable_param(0, 14, 100, "input_angle_vanila", 'crossings_angle', 'log_files_vanila', 'crossings_angle_simple_gradient.sh') #start from input 1 for 2018
#create_qsub_variable_param(0, 14, 100, "input_angle_new_GDs", 'crossings_angle', 'log_files_new_GDs', 'crossings_angle_new_GDs.sh', 30, 10, 100, 50)
#create_qsub_variable_param(0, 14, 100, "input_angle_new_GDs", 'crossings_angle', 'log_files_new_GDs', 'crossings_angle_new_GDs.sh', 30, 2, 100, 2)
create_qsub_variable_param(4, 14, 100, "input_angle_new_GDs", 'crossings_angle', 'log_files_new_GDs', 'crossings_angle_new_GDs.sh', 'crossings_angle_new_GDs_mem.sh', 30, 2, 100, 2)

#create_qsub_variable_param(0, 16, 100, "input_angle_17") #start from input 1
#create_qsub_variable_param(2, 20)

