import numpy as np
import os
from input_functions import *

def write_array(arr, file_name):
 file = open(file_name,"w")
 for j in range(len(arr)):
  file.write(str(j+1)+"\t"+str(arr[j])+"\n")
 file.close()

def check_grad_desc(number_of_inputs, edge_threshold, folder_name):
 #folder_name = "input_angle"
 #folder_name = "../graphs/input18"
 file_names = []
 for i in range(number_of_inputs):
  file_names.append("input"+str(i+1))
 #file_names = ["input1", "input2", "input3", "input4", "input5", "input6"]

 W_start = 1
 W_end = 2

 #K_start = -5
 #K_end = 6

 K_start = -3
 K_end = 3

 NUM_RUNS = 6
 NUM_ITERATIONS = 3

 command_count = 0

 max_min_angle = []

 #for n in range(2):
 for n in range(1):
  for W in range(W_start, W_end):
   for i in range(len(file_names)):
    max_min_angle.append(-1)
    print(folder_name + '/' + file_names[i]+'.txt')
    dummy, coord_list, edge_list = take_input(folder_name + '/' + file_names[i]+'.txt')
    if len(edge_list)>edge_threshold:
     NUM_RUNS = 2
     NUM_ITERATIONS = 2
     for K in range(K_start,K_end):
      command_count = command_count + 1
      K_start_str = str(K)
      if K<0:
       K_start_str = 'N'+str(-K)
      K_end_str = str(K+1)
      if K+1<0:
       K_end_str = 'N'+str(-K-1)
      if not os.path.exists(folder_name + '/' + file_names[i] + '_ang_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy'):
       continue
      MIN_ANGLES = np.load(folder_name + '/' + file_names[i] + '_ang_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
      X_new = np.load(folder_name + '/' + file_names[i] + '_xy_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
      for nr in range(NUM_RUNS):
       if max_min_angle[i]<MIN_ANGLES[W-W_start][0][nr]:
        max_min_angle[i] = MIN_ANGLES[W-W_start][0][nr]
        write_as_txt(folder_name + '/grad_desc' + str(i+1) + '.txt', edge_list, X_new[W-W_start][0][nr][:,0], X_new[W-W_start][0][nr][:,1])
        txt_to_json(folder_name + '/grad_desc' + str(i+1) + '.txt', folder_name + '/grad_desc' + str(i+1) + '.json')
    else:
     NUM_RUNS = 6
     NUM_ITERATIONS = 3
     command_count = command_count + 1
     K_start_str = str(K_start)
     if K_start<0:
      K_start_str = 'N'+str(-K_start)
     K_end_str = str(K_end)
     if K_end<0:
      K_end_str = 'N'+str(-K_end)
     if not os.path.exists(folder_name + '/' + file_names[i] + '_ang_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy'):
      continue
     MIN_ANGLES = np.load(folder_name + '/' + file_names[i] + '_ang_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
     X_new = np.load(folder_name + '/' + file_names[i] + '_xy_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
     for K in range(K_start,K_end):
      for nr in range(NUM_RUNS):
       #print(MIN_ANGLES.shape)
       #print(MIN_ANGLES)
       #print(str(W))
       #print(str(K-K_start))
       #print(str(nr))
       if max_min_angle[i]<MIN_ANGLES[W-W_start][K-K_start][nr]:
        max_min_angle[i] = MIN_ANGLES[W-W_start][K-K_start][nr]
        write_as_txt(folder_name + '/grad_desc' + str(i+1) + '.txt', edge_list, X_new[W-W_start][K-K_start][nr][:,0], X_new[W-W_start][K-K_start][nr][:,1])
        txt_to_json(folder_name + '/grad_desc' + str(i+1) + '.txt', folder_name + '/grad_desc' + str(i+1) + '.json')

 write_array(max_min_angle, folder_name + '/grad_desc_max_min_angle.txt')
 print(max_min_angle)


def check_grad_desc_ncr(edge_threshold, folder_name):
 my_inf = 10000000
 #folder_name = "input_angle"
 #folder_name = "../graphs/input18"
 file_names = []
 for name in os.listdir(folder_name):
  if name=='.gml.txt':
   continue
  if name[:4]=='grad':
   continue
  if name[len(name)-4:len(name)]=='.txt':
   file_names.append(name[:len(name)-4])
 #file_names = ["input1", "input2", "input3", "input4", "input5", "input6"]

 W_start = 1
 W_end = 2

 #K_start = -5
 #K_end = 6

 K_start = -3
 K_end = 3

 NUM_RUNS = 6
 NUM_ITERATIONS = 3

 command_count = 0

 min_ncr = []

 #for n in range(2):
 for n in range(1):
  for W in range(W_start, W_end):
   for i in range(len(file_names)):
    min_ncr.append(my_inf)
    dummy, coord_list, edge_list = take_input(folder_name + '/' + file_names[i]+'.txt')
    if len(edge_list)>edge_threshold:
     NUM_RUNS = 2
     NUM_ITERATIONS = 2
     for K in range(K_start,K_end):
      command_count = command_count + 1
      K_start_str = str(K)
      if K<0:
       K_start_str = 'N'+str(-K)
      K_end_str = str(K+1)
      if K+1<0:
       K_end_str = 'N'+str(-K-1)
      if not os.path.exists(folder_name + '/' + file_names[i] + '_ncr_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy'):
       continue
      MIN_NCR = np.load(folder_name + '/' + file_names[i] + '_ncr_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
      X_new = np.load(folder_name + '/' + file_names[i] + '_xy_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
      for nr in range(NUM_RUNS):
       if min_ncr[i]>MIN_NCR[W-W_start][0][nr]:
        min_ncr[i] = MIN_NCR[W-W_start][0][nr]
        write_as_txt(folder_name + '/grad_desc' + str(i+1) + '.txt', edge_list, X_new[W-W_start][0][nr][:,0], X_new[W-W_start][0][nr][:,1])
        txt_to_json(folder_name + '/grad_desc' + str(i+1) + '.txt', folder_name + '/grad_desc' + str(i+1) + '.json')
    else:
     NUM_RUNS = 6
     NUM_ITERATIONS = 3
     command_count = command_count + 1
     K_start_str = str(K_start)
     if K_start<0:
      K_start_str = 'N'+str(-K_start)
     K_end_str = str(K_end)
     if K_end<0:
      K_end_str = 'N'+str(-K_end)
     if not os.path.exists(folder_name + '/' + file_names[i] + '_ncr_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy'):
      continue
     MIN_NCR = np.load(folder_name + '/' + file_names[i] + '_ncr_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
     X_new = np.load(folder_name + '/' + file_names[i] + '_xy_'+'W_'+str(W)+'_NORM_'+str(n)+'_K_'+K_start_str+'_'+K_end_str+'.npy')
     for K in range(K_start,K_end):
      for nr in range(NUM_RUNS):
       #print(MIN_ANGLES.shape)
       #print(MIN_ANGLES)
       #print(str(W))
       #print(str(K-K_start))
       #print(str(nr))
       if min_ncr[i]>MIN_NCR[W-W_start][K-K_start][nr]:
        min_ncr[i] = MIN_NCR[W-W_start][K-K_start][nr]
        write_as_txt(folder_name + '/grad_desc' + str(i+1) + '.txt', edge_list, X_new[W-W_start][K-K_start][nr][:,0], X_new[W-W_start][K-K_start][nr][:,1])
        txt_to_json(folder_name + '/grad_desc' + str(i+1) + '.txt', folder_name + '/grad_desc' + str(i+1) + '.json')

 write_array(min_ncr, folder_name + '/grad_desc_min_ncr.txt')
 incomplete_graphs = []
 if my_inf in min_ncr:
  for i in range(len(min_ncr)):
   if my_inf == min_ncr[i]:
    incomplete_graphs.append(i)
 for i in range(len(incomplete_graphs)):
  print(file_names[incomplete_graphs[i]])
 min_ncr.sort()
 #min_ncr = min_ncr[:85]
 print(min_ncr)
 print(sum(min_ncr) / len(min_ncr))
 print(min_ncr[0], min_ncr[int(len(min_ncr)*.25)], min_ncr[int(len(min_ncr)*.50)], min_ncr[int(len(min_ncr)*.75)], min_ncr[len(min_ncr)-1])
 return min_ncr


#check_grad_desc(16, 100)
#check_grad_desc(10, 100)

#check_grad_desc(11, 100, '../graphs/input_angle/')
#check_grad_desc(16, 100, '../graphs/input_angle_17/')
#check_grad_desc_ncr(11, 100, '../graphs/input_crossing/')

#check_grad_desc_ncr(100, '../graphs/community')
#check_grad_desc_ncr(100, '../graphs/north')
#check_grad_desc_ncr(100, '../graphs/plantri')
#check_grad_desc_ncr(100, '../graphs/rome')

def check_all():
 arr = []
 arr.extend(check_grad_desc_ncr(100, '../graphs/community'))
 arr.extend(check_grad_desc_ncr(100, '../graphs/north'))
 arr.extend(check_grad_desc_ncr(100, '../graphs/plantri'))
 arr.extend(check_grad_desc_ncr(100, '../graphs/rome'))
 arr.sort()
 print(sum(arr) / len(arr))
 print(arr[0], arr[int(len(arr)*.25)], arr[int(len(arr)*.50)], arr[int(len(arr)*.75)], arr[len(arr)-1])

check_all()


