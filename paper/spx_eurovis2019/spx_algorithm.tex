\section{SPX Algorithm}
We provide an algorithm that can reduce both the number of crossings in a graph as well as improve the maximum crossing angle of a straight line 2D embedding of a graph while preserving metrics such as stress of the graph. We adapt the approach from ~\cite{bennett2010} who optimize for both reducing stress and number of edge crossings using an optimization based approach. We modify the objective function so that it we maximize crossing angles while simultaneously reducing stress and number of edge crossings. While most works in present only optimize for one of these metrics, we optimize for all three of these metrics. We describe our general approach to optimizing graph aesthetics such as stress, number of edge crossing, and minimum crossing angle and present the results of our experiments on various graph classes as well as the graphs from the Graph Drawing Live Challenge 2017 and 2018.

We can express edge crossings as a system of non-linear constraints. Let us consider two edges $\textbf{A} = [a_1^x , a_1^y; a_2^x , a_2^y]$ and $\textbf{B} = [b_1^x , b_1^y; b_2^x , b_2^y]$ where the endpoints of $\textbf{A}$ are $(a_1^x , a_1^y)$, and $(a_2^x , a_2^y)$ and similarly for $\textbf{B}$. Any point on an edge can be expressed as a convex combination of its endpoints. Therefore, two edges do not intersect if and only if there exists no $\textbf{w}_\textbf{a}$ and $\textbf{w}_\textbf{b}$ such that $\textbf{A}'\textbf{w}_\textbf{a}$ = $\textbf{B}'\textbf{w}_\textbf{b}$, $\textbf{e}'\textbf{w}_\textbf{a} = \textbf{1}$, $\textbf{e}'\textbf{w}_\textbf{b} = \textbf{1}$, $\textbf{w}_\textbf{a} \geq \textbf{0}$, $\textbf{w}_\textbf{b} \geq \textbf{0}$, $\textbf{e} = (1,1)'$. 

This condition can be described using Farkas' Theorem which states that the edges $\textbf{A}$ and $\textbf{B}$ do not cross if and only if there exists $\textbf{u}$, $\alpha$ and $\beta$,
such that

\begin{equation}
\label{eq1}
\textbf{Au} \geq \alpha \textbf{e}, Bu \leq \beta \textbf{e}, \alpha - \beta > 0
\end{equation}

Since the magnitude of $\textbf{u}$ is a free parameter, we can set $\alpha - \beta = 1$ such that  
$\alpha = -\gamma$, and $\beta = -\gamma - 1$. We get

\begin{equation}
\label{eq2}
\textbf{Au} + \gamma \textbf{e} \geq \textbf{0}, \textbf{Bu} + (1+\gamma) \textbf{e} \leq \textbf{0}
\end{equation}

Therefore, two edges do not intersect if and only if 
\begin{equation}
\label{eq3}
 0 = min_{u,v}||(-Au -\gamma e)_+||_1 + ||(Bu + (1+\gamma)e_+||_1 where (z)_+ = max(0, z)
\end{equation}

We can combine this objective function with objective functions for other aesthetic criteria such as stress and crossing angle in a graph drawing to optimize all these criteria jointly.

\subsection{Stress plus Edge Crossing Minimization}

We combine the stress metric with Eq.\ref{eq3} and minimize both stress and edge crossings

\begin{equation}
\label{eq4}
Cost(X,u,\gamma, \rho) = min_{X,U,\gamma} stress(X) + \sum_{i=1}^{k} (\rho_i/2) * \{|| (-A^{i}(X)u^i - \gamma^{i}e)_{+}  ||_1 \\+ ||(B^i(X)u^i + (1+\gamma^i)e )_{+}||_1\}
\end{equation}

where, stress$(X) = \sum_{i<j} (w_{ij} ||X_i - X_j|| - dij)^2$, 

$d_{ij} = $ideal distance between node pair $i,j$ computed using all pairs shortest path algorithm,

$w_{ij} = d^{-2}_{ij}$,

$k = $ no. of edge pairs $= O(m^2)$, where $m$ is the number of edges

$A^i(X)$ and $B^i(X)$ extract the first and second edge for the edge pair $i$ as matrices $A$ and $B$

$u^i, \gamma^i$ are the $u,\gamma$ pair for the edge pair $i$,

$\rho_i$  is the weight for the penalty associated with the edge pair $i$

\begin{algorithm}
\caption{Minimize crossings($G$)}
\begin{algorithmic}
\State Compute the initial layout $X_0$ (using stress majorization, force-directed layout, or random layout)
\For{$NUM\_ITERS$}
\State Keeping $X$ constant, optimize $u$ and $\gamma$ for each edge pair using linear programming. Skip non-intersecting edge pair by setting $\rho = 0$
\State Keeping $u$ and $\gamma$ constant, modiy X to optimize the cost function using gradient descent
\EndFor

\end{algorithmic}

\end{algorithm}


\subsection{Stress plus Edge Crossing plus Crossing Angle Maximization}
We can jointly optimize for crossing angle, number of crossings, and stress under the SPX model using the following cost function

\begin{equation}
\label{eq5}
Cost(X,u,\gamma, \rho) = min_{X,U,\gamma} stress(X) + \sum_{i=1}^{k} (\rho_i/2) * cos^2(\theta)*\{|| (-A^{i}(X)u^i - \gamma^{i}e)_{+}  ||_1 + ||(B^i(X)u^i + (1+\gamma^i)e )_{+}||_1\}
\end{equation}

where $cos(\theta)$ is the cosine of the angle between the crossing edge pair.
We use $cos^2(theta)$ for the optimization because the crossing angle between an edge pair is a symmetrical relation.

\subsection{Optimization Details}
During the first phase of Algorithm \textbf{Minimize crossings}, we optimize for eq.\ref{eq3} using a linear program keeping the X's constant.

During the second phase of algorithm, we use gradient descent to optimize the cost function \ref{eq4} by modifying X and keeping u's and $\gamma$'s constant.

We experimented with many gradient descent algorithms including bfgs, lbfgs, vanilla gradient descent, momentum, nesterov momentum, rmsprop, adagrad, and adam optimizer.

We found vanilla and adam optimizers to be the best performing algorithms.

\subsubsection{Parameters}
We use parameters that combine the stress and edge penalties in the graph drawing.
We have,

stress$(X) = \sum_{i<j} (w_{ij} ||X_i - X_j|| - dij)^2$
and let us define sum of edge penalties as 

$edgePenalties(X,u,\gamma, \rho) = \sum_{i=1}^{k} (\rho_i/2) * \{|| (-A^{i}(X)u^i - \gamma^{i}e)_{+}  ||_1 \\+ ||(B^i(X)u^i + (1+\gamma^i)e )_{+}||_1\}$

Then the cost function $cost(X,u,\gamma, \rho) = stress(X) + edgePenalties(X,u,\gamma,\rho)$

We use hyperparameter $K$ to combine the stress and penalties and our new cost function becomes
$cost(X,u,\gamma, \rho) = stress(X) + K*edgePenalties(X,u,\gamma,\rho)$
We tried different values of K ranging from $[2^{-5}, 2^5]$ as well as in the range of $[10^0, 10^5]$ in logarithmic increments.

\subsection{Normalization}
The edge penalties that were computed using the $u$s, and $\gamma$s for any edge pair were found to be in the range $[0,2]$, where we obtained a penalty of 0 for non-crossing pairs and values between $[1,2]$ for crossing pairs.
Since stress of a function can be orders of magnitude bigger than the sum of penalties which individually are in the range $[0,2]$, we normalize the stress and penalties using 
the number of edges $m$ in the graph.

\subsubsection{Different Initial layouts as input}
We run our experiments using 3 different layout algorithms as input to the SPX algorithm. This includes stress majorization (neato), force-directed layout (sfdp), and random initialization. To prevent the effects of sensitivity to initial layout, we repeat the experiments multiple times for each of the initial layout. 

\subsubsection{Gradient Descent Algorithms}
We used different algorithms for gradient descent (gd). We used algorithms ranging from bfgs, l-bfgs, vanilla gradient descent, momentum-based gd, nesterov momentum-based gd, adagrad, rmsprop, and adam.

For bfgs, we investigated using both numerical and analytical gradient, and noticed an improvement in runtime(50x) and performance using analytical gradient.
For other algorithms, we tuned various hyperparameters such as number of iterations, learning rate $\alpha$.


\subsection{Sweepline algorithm for fast intersection detection}
We can detect all pairs of crossing edges using a brute force search of $O(m^2)$, where $m$ is the number of edges. Using this naive strategy incurred us huge runtime for large graphs ($> 1000$ edges ). To speed up the crossing detection, we use a variation of bentley-ottman sweep-line algorithm which can also handle overlapping edges, and horizontal, and vertical segments.

\subsection{Stochastic Gradient Descent}
Gradient Descent uses information from every crossing edge pair in the graph which causes the update of the coordinates to be slow. To speed up the runtime performance of the gradient descent, we investigate stochastic gradient descent by sampling edge pairs. 

\subsection{Sampling Strategy}

\subsubsection{Naive Sampling}
Naive sampling chooses N edge pairs at random, where N is determined by the desired runtime of the optimization. Naive sampling choosing N edge pairs uniformly at random from all possible edge pairs for further processing.

\subsubsection{Bucket Sampling}
Our bucket sampling strategy first bins the edges based on slope of the edges. When sampling edge pairs, we choose edge pairs from the same or nearby bucket with higher probability compared to buckets that are farther from each other. This increases the probability of selecting the edge pair that have the minimum crossing angle in the graph and hence our stochastic gradient descent can optimize the minimum crossing angle.
