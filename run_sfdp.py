import subprocess
import sys
from random import randint
import time

def run_algo(output_file, algo_name, folder_name, attributes):
 #p = subprocess.Popen([algo_name, "-Goverlap=prism", "-Goutputorder=edgesfirst", "-Gstart=123", "-Gsize=50,50!", folder_name+"/"+input_file], stdout=subprocess.PIPE)
 arr = []
 arr.append(algo_name)
 for a in attributes:
  arr.append(a)
 #print(arr)
 p = subprocess.Popen(arr, stdout=subprocess.PIPE)

 file = open(folder_name+"/"+output_file, 'w')
 output, error = p.communicate()
 file.write(output)
 #print(output)
 #print(error)
 file.close()

def run_sfdp(input_file, output_file, folder_name):
 #According to Iqbal, sfdp is random if start parameter is not provided or it is set with random numbers. Hence, I am just removing start
 #run_algo(input_file, output_file, "sfdp", folder_name, ["-Goverlap=prism", "-Goutputorder=edgesfirst", "-Gstart=123", "-Gsize=50,50!", folder_name+"/"+input_file])
 # however that does not work, hence try other one
 #run_algo(input_file, output_file, "sfdp", folder_name, ["-Goverlap=prism", "-Goutputorder=edgesfirst", "-Gsize=50,50!", folder_name+"/"+input_file])
 run_algo(output_file, "sfdp", folder_name, ["-Goverlap=prism", "-Goutputorder=edgesfirst", "-Gstart="+str(randint(0, 10000))+"", "-Gsize=50,50!", folder_name+"/"+input_file])

def run_sfdp_all(folder_name):
 for i in range(1,101):
  run_sfdp("graph"+str(i)+".dot", "sfdp_layout"+str(i)+".dot", folder_name)


def run_neato(input_file, output_file, folder_name):
 # the first command was the previous one, but I noticed that there are two -Gstart, I really want random embedding every time, hence, I am deleting the first one.
 #run_algo(input_file, output_file, "neato", folder_name, ["-Goverlap=prism", "-Goutputorder=edgesfirst", "-Gstart=123", "-Gsize=50,50!", "-Gstart=random", folder_name+"/"+input_file])
 #run_algo(input_file, output_file, "neato", folder_name, ["-Goverlap=prism", "-Goutputorder=edgesfirst", "-Gsize=50,50!", "-Gstart=random", folder_name+"/"+input_file])
 # just -Gstart=random does not work
 run_algo(output_file, "neato", folder_name, ["-Goverlap=prism", "-Goutputorder=edgesfirst", "-Gstart="+str(randint(0, 10000))+"", "-Gsize=50,50!", folder_name+"/"+input_file])

def run_neato_all(folder_name):
 for i in range(1,101):
  run_neato("graph"+str(i)+".dot", "neato_layout"+str(i)+".dot", folder_name)

def run_neato_for_all_graph_multiple_times(folder_name, number_of_graphs, number_of_drawings, file_name_prefix):
 for i in range(1, number_of_graphs+1):
  for j in range(1, number_of_drawings+1):
   run_neato(file_name_prefix+str(i)+".dot", "neato_layout"+str(i)+"_drawing_"+str(j)+".dot", folder_name)

def run_sfdp_for_all_graph_multiple_times(folder_name, number_of_graphs, number_of_drawings, file_name_prefix):
 for i in range(1, number_of_graphs+1):
  for j in range(1, number_of_drawings+1):
   run_sfdp(file_name_prefix+str(i)+".dot", "sfdp_layout"+str(i)+"_drawing_"+str(j)+".dot", folder_name)

def usage():
 if len(sys.argv)<5:
  print('usage:python run_sfdp.py folder_name number_of_graphs number_of_drawings file_name_prefix')
  quit()
 else:
  return sys.argv[1], int(sys.argv[2]), int(sys.argv[3]), sys.argv[4]


folder_name, number_of_graphs, number_of_drawings, file_name_prefix = usage()
#run_sfdp_all(folder_name)
#run_neato_all(folder_name)
start_time = time.time()
run_neato_for_all_graph_multiple_times(folder_name, number_of_graphs, number_of_drawings, file_name_prefix)
print('Time to complete run_neato_for_all_graph_multiple_times: ' + str((time.time() - start_time)) + ' seconds')
start_time = time.time()
run_sfdp_for_all_graph_multiple_times(folder_name, number_of_graphs, number_of_drawings, file_name_prefix)
print('Time to complete run_sfdp_for_all_graph_multiple_times: ' + str((time.time() - start_time)) + ' seconds')

def run_neato_for_files_multiple_times(folder_name, file_names, number_of_drawings):
 for i in range(len(file_names)):
  for j in range(number_of_drawings):
   run_neato(file_names[i]+".dot", 'run_neato_'+file_names[i]+'_'+str(j)+'.dot', folder_name)

#run_neato_for_files_multiple_times("meeting_08_21_2018/todo2", ["er_8_0.6", "er_10_0.6", "er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"], 5)
#run_neato_for_files_multiple_times("meeting_08_21_2018/todo2", ["er_5_0.6", "er_6_0.6", "er_7_0.6"], 5)
#arr = []
#for i in range(number_of_graphs):
# arr.append(file_name_prefix+str(i+1))
#run_neato_for_files_multiple_times(folder_name, arr, 5)
 
