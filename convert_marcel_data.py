from input_functions import *
import os

def convert_all(folder_name, output_folder):
  count = 0
  for name in os.listdir(folder_name):
    count += 1
    gml_to_txt(folder_name+name, output_folder+name[:len(name)-4]+'.txt')
  print(count)

root_folder = '/Users/abureyanahmed/Downloads/crossing_min_random_drawing/'
#convert_all(root_folder + 'community/', '../graphs/community/')
convert_all(root_folder + 'north/', '../graphs/north/')
convert_all(root_folder + 'plantri/', '../graphs/plantri/')
convert_all(root_folder + 'rome/', '../graphs/rome/')

