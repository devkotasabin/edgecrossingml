from input_functions import *
import subprocess
import sys
from random import randint
import time

def run_algo(algo_name, attributes):
 arr = []
 arr.append(algo_name)
 for a in attributes:
  arr.append(a)
 #print(arr)
 p = subprocess.Popen(arr, stdout=subprocess.PIPE)

 output, error = p.communicate()
 #print(output)
 #print(error)

def run_coffeeVM(input_file, output_file, folder_name, runs, out_csv):
 run_algo("/Users/abureyanahmed/Downloads/CoffeeVM-marcel/src/build/CoffeeVM/CoffeeVM", ["-inputfile", folder_name+"/"+input_file, "-gml", folder_name+"/"+output_file, "-runs", runs, "-output", folder_name+"/"+out_csv])

def convert_files(folder_name, number_of_inputs):
 for i in range(number_of_inputs):
  txt_to_gml(folder_name + '/input'+str(i+1)+'.txt', folder_name + '/input'+str(i+1)+'.gml')

def run_inputs(folder_name, number_of_inputs):
 for i in range(number_of_inputs):
  run_coffeeVM('input'+str(i+1)+'.gml', 'output'+str(i+1)+'.gml', folder_name, "10", 'output'+str(i+1)+'.csv')

def get_results(folder_name, number_of_inputs):
 for i in range(number_of_inputs):
  file = open(folder_name + '/output'+str(i+1)+'.csv', 'r')
  is_first = True
  max_res = 0
  run_time = 0
  while True:
   l = file.readline()
   if len(l) == 0:
    break
   if not is_first:
    arr = l.split(';')[7:9]
    if max_res < float(arr[0]):
     max_res = float(arr[0])
     run_time = float(arr[1])
   else:
    is_first = False
  print('Resolution: ', max_res)
  print('Time: ', run_time)
  file.close()

def main():
 #convert_files('../graphs/input', 16)
 #run_inputs('../graphs/input', 16)
 #get_results('../graphs/input', 16)
 #convert_files('../graphs/input18', 14)
 #run_inputs('../graphs/input18', 14)
 get_results('../graphs/input18', 13)

main()


