import subprocess
from utils import *
from input_functions import *

def force_directed(input_file, output_file, iterations, is_repulsion_crossing_enabled, is_random):
 p = subprocess.Popen(["node", "force_directed.js", input_file, output_file, iterations, is_repulsion_crossing_enabled, is_random], stdout=subprocess.PIPE)
 output, error = p.communicate()


#force_directed("../graphs/input/input2.txt", "../graphs/input/force_directed2.txt", "100", "repulsion_crossing", "random")
#force_directed("../graphs/input/input2.txt", "../graphs/input/force_directed2.txt", "100", "no_repulsion_crossing", "input")

def write_array(arr, file_name):
 file = open(file_name,"w")
 for j in range(len(arr)):
  file.write(str(j+1)+"\t"+str(arr[j])+"\n")
 file.close()

def write_str_array(arr, file_name):
 file = open(file_name,"w")
 for j in range(len(arr)):
  file.write(str(j+1)+"\t"+arr[j]+"\n")
 file.close()

def one_drawing(folder_name, is_random, is_repulsion_crossing_enabled, d, input_file, max_min_angle, i, max_min_angle_output_file):
 output_file = folder_name + 'force_directed' + str(d+1)
 iterations = "500"
 force_directed(input_file, output_file+ '.txt', iterations, is_repulsion_crossing_enabled, is_random)
 ma = min_angle(output_file+ '.txt')
 if(max_min_angle[i]<ma):
  max_min_angle[i] = ma
  max_min_angle_output_file[i] = output_file
 d = d + 1
 return d

def force_directed_all(folder_name, input_prefix):
 if folder_name[len(folder_name)-1]!='/':
  folder_name = folder_name + '/'
 input_arr = []
 output_arr = []
 number_of_files = 14
 number_of_try = 4
 max_min_angle_output_file = []
 max_min_angle = []


 for i in range(number_of_files):
  json_to_txt(folder_name + input_prefix + str(i+1) + '.json', folder_name + input_prefix + str(i+1) + '.txt')
  input_file = folder_name + input_prefix + str(i+1) + '.txt'
  n, coord_list, edge_list = take_input(input_file)
  if len(edge_list)>1000:
   number_of_try = 1
  elif len(edge_list)>500:
   number_of_try = 2
  else:
   number_of_try = 4
  max_min_angle_output_file.append('')
  max_min_angle.append(-1)

  # input as initial layout
  d = 0 # first drawing

  d = one_drawing(folder_name, "input", "no_repulsion_crossing", d, input_file, max_min_angle, i, max_min_angle_output_file)

  d = one_drawing(folder_name, "input", "repulsion_crossing", d, input_file, max_min_angle, i, max_min_angle_output_file)

  for j in range(number_of_try):
   d = one_drawing(folder_name, "random", "no_repulsion_crossing", d, input_file, max_min_angle, i, max_min_angle_output_file)

  for j in range(number_of_try):
   d = one_drawing(folder_name, "random", "repulsion_crossing", d, input_file, max_min_angle, i, max_min_angle_output_file)
   
  print('Completed input '+str(i+1))
  print('Max min angle: '+str(max_min_angle[i]))
  txt_to_json(max_min_angle_output_file[i] + '.txt', max_min_angle_output_file[i] + '.json')
  print('Output file name: '+max_min_angle_output_file[i] + '.json')

 write_array(max_min_angle, folder_name+'force_directed_max_min_angle.txt')
 write_str_array(max_min_angle_output_file, folder_name+'force_directed_max_min_angle_output_file.txt')

#force_directed_all('../graphs/input/', 'input')
force_directed_all('../graphs/input18/', 'input')

