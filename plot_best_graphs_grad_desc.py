import numpy as np
from input_functions import *
from utils import *

def draw_graphs(number_of_inputs, folder_name):
 for i in range(number_of_inputs):
 #for i in range(9):
 #for i in range(1):
  #filename_txt = "../graphs/input18" + '/grad_desc' + str(i+1) + '.txt'
  #filename_img = "../graphs/layout18" + '/grad_desc' + str(i+1) + '.png'
  filename_txt = folder_name + '/grad_desc' + str(i+1) + '.txt'
  filename_img = folder_name + '/grad_desc' + str(i+1) + '.png'
  n, coord_list, edge_list = take_input(filename_txt)
  print(n,coord_list)
  x = []
  y = []
  for j in range(n):
   x.append(coord_list[j][0])
   y.append(coord_list[j][1])
  draw_graph(x, y, edge_list, filename_img)


#draw_graphs(11, '../graphs/input_angle')
#draw_graphs(16, '../graphs/input_angle_17')
#draw_graphs(11, '../graphs/input_crossing')
#draw_graphs(16, '../graphs/input_crossing_17')

#draw_graphs(11, '../graphs/input_angle_vanila')
#draw_graphs(11, '../graphs/input_angle_adam')
draw_graphs(2, '../graphs/input_angle_new_GDs')


