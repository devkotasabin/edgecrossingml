import json
import matplotlib.pyplot as plt

my_inf = 1000000000
fig_number = 0

def plot_data_from_file(file_name, xlabel, ylabel, title, output_figure_name, initial_result_file_name):
 global my_inf, fig_number
 with open(file_name) as f:
  data = json.load(f)
 #print('length of data:'+str(len(data)))
 #print('length of data row:'+str(len(data[0])))
 min_crossings = [my_inf for i in range(len(data[0]))]
 max_crossings = [-1 for i in range(len(data[0]))]
 for i in range(len(data)):
  for j in range(len(data[0])):
   if data[i][j]<min_crossings[j]:min_crossings[j]=data[i][j]
   if data[i][j]>max_crossings[j]:max_crossings[j]=data[i][j]
 #fig = plt.figure()
 #ax = plt.axes()
 #ax.set_xlabel(xlabel)
 #ax.set_ylabel(ylabel)
 #ax.set_title(title)
 #ax.plot(min_crossings)
 #plt.savefig(output_figure_name, format="png")

 with open(initial_result_file_name) as f:
  data_init = json.load(f)

 fig = plt.figure(fig_number)
 fig_number = fig_number + 1
 plt.plot(range(1, len(data[0])+1), min_crossings, 'r--', label='min')
 plt.plot(range(1, len(data[0])+1), max_crossings, 'b^', label='max')
 plt.plot(range(1, len(data_init)+1), data_init, 'go', label='input')
 plt.legend(loc=0)
 plt.title(title)
 plt.ylabel(ylabel)
 plt.xlabel(xlabel)
 plt.savefig(output_figure_name)
 plt.close(fig)


#plot_data_from_file('neato_crossings.txt', 'Graphs (|V|=100)', 'Crossings', 'NEATO layout, Erdos Renyi', 'neato.png')
#plot_data_from_file('sfdp_crossings.txt', 'Graphs (|V|=100)', 'Crossings', 'SFDP layout, Erdos Renyi', 'sfdp.png')

plot_data_from_file('../graphs/input/sfdp_crossings.txt', 'Graphs', 'Crossings', 'SFDP layouts for last year contest graphs', 'sfdp.png', '../graphs/input/input_crossings.txt')
plot_data_from_file('../graphs/input/neato_crossings.txt', 'Graphs', 'Crossings', 'NEATO layouts for last year contest graphs', 'neato.png', '../graphs/input/input_crossings.txt')
plot_data_from_file('../graphs/input/sfdp_min_angles.txt', 'Graphs', 'Angle', 'SFDP layouts for last year contest graphs', 'sfdp_angle.png', '../graphs/input/input_min_angles.txt')
plot_data_from_file('../graphs/input/neato_min_angles.txt', 'Graphs', 'Angle', 'NEATO layouts for last year contest graphs', 'neato_angle.png', '../graphs/input/input_min_angles.txt')

