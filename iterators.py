import numpy as np

def jacob_iter(folder_name, my_func):
 #file_names = ["er_8_0.6", "er_10_0.6", "er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
 #file_names = ["er_5_0.6", "er_6_0.6", "er_7_0.6"]
 #file_names = ["er_50_0.1", "er_50_0.2", "er_100_0.1", "er_100_0.2", "er_150_0.1", "er_150_0.2", "er_200_0.1", "er_200_0.2"]
 #file_names = ["input1", "input2"]
 file_names = ["input1", "input2", "input3", "input4", "input5", "input6"]

 W_start = 1
 W_end = 2

 #K_start = -5
 #K_end = 6

 K_start = -3
 K_end = 3

 NUM_RUNS = 3
 NUM_ITERATIONS = 6

 NUMBER_OF_CROSSINGS = []
 COST_FUNCTIONS = []
 X_new = []
 init_X = []

 for n in range(2):
 #for n in range(1):
  NUMBER_OF_CROSSINGS.append([])
  COST_FUNCTIONS.append([])
  X_new.append([])
  init_X.append([])
  for i in range(len(file_names)):
   NUMBER_OF_CROSSINGS[n].append([])
   COST_FUNCTIONS[n].append([])
   X_new[n].append([])
   init_X[n].append([])
   for W in range(W_start, W_end):
    #NUMBER_OF_CROSSINGS[n][i].append([])
    #COST_FUNCTIONS[n][i].append([])
    #X_new[n][i].append([])
    #init_X[n][i].append([])
    #for K in range(K_start,K_end):
    # res_arr = my_func(folder_name, file_names, i, W, K, n, K_start, NUM_RUNS, NUM_ITERATIONS)
    # NUMBER_OF_CROSSINGS[n][i][W-W_start].append(res_arr[0])
    # COST_FUNCTIONS[n][i][W-W_start].append(res_arr[1])
    # X_new[n][i][W-W_start].append(res_arr[2])
    # init_X[n][i][W-W_start].append(res_arr[3])
    res_arr = my_func(folder_name, file_names, i, W, K_start, K_end, n, NUM_RUNS, NUM_ITERATIONS)
    NUMBER_OF_CROSSINGS[n][i].append(res_arr[0])
    COST_FUNCTIONS[n][i].append(res_arr[1])
    X_new[n][i].append(res_arr[2])
    init_X[n][i].append(res_arr[3])

 return NUMBER_OF_CROSSINGS, COST_FUNCTIONS, X_new, init_X


#def jacob_write(folder_name, file_names, i, W, K, n, K_start, NUM_RUNS, NUM_ITERATIONS):
def jacob_write(folder_name, file_names, i, W, K_start, K_end, n, NUM_RUNS, NUM_ITERATIONS):
 f = open('qsub_commands_jacob.sh','a')
 f.write('export OUTPUT_FOLDER='+folder_name+'\n')
 f.write('export FILE_NAME_PREFIX='+file_names[i]+'\n')
 f.write('export W_start='+str(W)+'\n')
 f.write('export W_end='+str(W+1)+'\n')
 f.write('export K_start='+str(K_start)+'\n')
 f.write('export K_end='+str(K_end)+'\n')
 f.write('export NUM_RUNS='+str(NUM_RUNS)+'\n')
 f.write('export num_iterations='+str(NUM_ITERATIONS)+'\n')
 f.write('export NORMALIZE='+str(n)+'\n')
 f.write('export OUTPUT_FILE_EXT='+'_'+'W_'+str(W)+'_NORM_'+str(n)+'\n')
 f.write('qsub -N edge_crossings -o log_files_angle/edge_crossings_'+file_names[i]+'_'+'W_'+str(W)+'_NORM_'+str(n)+'.out -e log_files_angle/edge_crossings_'+file_names[i]+'_'+'W_'+str(W)+'_NORM_'+str(n)+'.err -V edge_crossings_param.sh\n')
 f.close()
 return [0, 0, 0, 0]

def jacob_read(folder_name, file_names, i, W, K_start, K_end, n, NUM_RUNS, num_iterations):
 res_arr = []
 res_arr.append(np.load(folder_name + '/' + file_names[i] + '_ncr_'+'_'+'W_'+str(W)+'_NORM_'+str(n) + '.npy')[0])
 res_arr.append(np.load(folder_name + '/' + file_names[i] + '_cost_'+'_'+'W_'+str(W)+'_NORM_'+str(n) + '.npy')[0])
 res_arr.append(np.load(folder_name + '/' + file_names[i] + '_xy_'+'_'+'W_'+str(W)+'_NORM_'+str(n) + '.npy')[0])
 res_arr.append(np.load(folder_name + '/' + file_names[i] + '_init_xy_'+'_'+'W_'+str(W)+'_NORM_'+str(n) + '.npy')[0])
 return res_arr

#jacob_iter(jacob_read)
