# Show the result of a simple bow-tie quad
import poly_point_isect
from edge_crossing import *
from input_functions import *
from scipy.special import comb

def make_segment(p1x, p1y, p2x, p2y):
 segment = (p1x, p1y)
 segment = (segment,) + ((p2x, p2y),)
 return segment

def number_of_intersections(file_name):
 #print(file_name)
 if file_name.endswith('.dot'):
  node_coords, edge_list = parse_dot_file(file_name)
 elif file_name.endswith('.txt'):
  n, node_coords, edge_list = take_input(file_name)
 count = 0
 segment_arr = []
 for i in range(len(edge_list)):
  edge1 = edge_list[i]
  segment_arr.append(make_segment(node_coords[edge1[0]][0], node_coords[edge1[0]][1], node_coords[edge1[1]][0], node_coords[edge1[1]][1]))
 #print(segment_arr)
 print('counting... poly_point_isect.isect_segments')
 isect = poly_point_isect.isect_segments(segment_arr)
 print('counting...')
 isect = poly_point_isect.isect_segments_include_segments(segment_arr)
 for x in isect:
  intersection, segment_pair = x
  #seg1, seg2 = segment_pair
  seg1 = segment_pair[0]
  seg2 = segment_pair[1]
  p1, q1 = seg1
  p1x, p1y = p1
  q1x, q1y = q1
  p2, q2 = seg2
  p2x, p2y = p2
  q2x, q2y = q2
  if(doIntersect(p1x, p1y, q1x, q1y, p2x, p2y, q2x, q2y)):
   if len(segment_pair)==2:
    count = count + 1
   else:
    count = count + comb(len(segment_pair), 2)
 #print(isect)
 return count

#arr = []
#for i in range(1,17):
# arr.append(number_of_intersections('../graphs/input/input'+str(i)+'.txt'))
#print(arr)

#print(number_of_intersections('../graphs/input/input8.txt'))
#print(number_of_intersections('../graphs/input/input11.txt'))
#print(number_of_intersections('/Users/abureyanahmed/Downloads/fdp1.dot'))
#print(number_of_intersections('/Users/abureyanahmed/Downloads/T2.txt'))

debug_arr2 = []
def number_of_intersections_layout_statistics(file_name):
 #print(file_name)
 print('naive')
 if file_name.endswith('.dot'):
  node_coords, edge_list = parse_dot_file(file_name)
 elif file_name.endswith('.txt'):
  n, node_coords, edge_list = take_input(file_name)
 count = 0
 for i in range(len(edge_list)):
  for j in range(i+1,len(edge_list)):
   edge1 = edge_list[i]
   edge2 = edge_list[j]
   #if not share_vertex(edge1, edge2):
   if(doIntersect(node_coords[edge1[0]][0], node_coords[edge1[0]][1], node_coords[edge1[1]][0], node_coords[edge1[1]][1], node_coords[edge2[0]][0], node_coords[edge2[0]][1], node_coords[edge2[1]][0], node_coords[edge2[1]][1])):
    debug_arr2.append([node_coords[edge1[0]][0], node_coords[edge1[0]][1], node_coords[edge1[1]][0], node_coords[edge1[1]][1], node_coords[edge2[0]][0], node_coords[edge2[0]][1], node_coords[edge2[1]][0], node_coords[edge2[1]][1]])
    count = count + 1
    print(str(edge1)+" intersects "+str(edge2))
 return count

#print(number_of_intersections_layout_statistics('../graphs/input/input8.txt'))
#print(number_of_intersections_layout_statistics('../graphs/input/input11.txt'))
#print(number_of_intersections_layout_statistics('/Users/abureyanahmed/Downloads/fdp1.dot'))
print(number_of_intersections_layout_statistics('/Users/abureyanahmed/Downloads/T2.txt'))

#print(len(debug_arr1))
#print(len(debug_arr2))

#for i in range(len(debug_arr2)):
# found = 0
# for j in range(len(debug_arr1)):
#  #
# if found==0:
#  print
