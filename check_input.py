import subprocess
from utils import *
from input_functions import *

def write_array(arr, file_name):
 file = open(file_name,"w")
 for j in range(len(arr)):
  file.write(str(j+1)+"\t"+str(arr[j])+"\n")
 file.close()

def check_input(folder_name, input_prefix):
 if folder_name[len(folder_name)-1]!='/':
  folder_name = folder_name + '/'
 input_arr = []
 output_arr = []
 number_of_files = 16
 min_angle_arr = []


 for i in range(number_of_files):
  json_to_txt(folder_name + input_prefix + str(i+1) + '.json', folder_name + input_prefix + str(i+1) + '.txt')
  input_file = folder_name + input_prefix + str(i+1) + '.txt'
  n, coord_list, edge_list = take_input(input_file)
  min_angle_arr.append(min_angle(input_file))
   
 print('Checked all input.')

 write_array(min_angle_arr, folder_name+'input_min_angle.txt')

#check_input('../graphs/input/', 'input')
check_input('../graphs/input18/', 'input')

