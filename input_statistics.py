from input_functions import *

def input_stats(input_path, number_of_inputs):
  number_of_vertices = []
  number_of_edges = []
  for i in range(number_of_inputs):
    n, coord_list, edge_list = take_input(input_path+str(i+1)+'.txt')
    number_of_vertices.append(n)
    number_of_edges.append(len(edge_list))
  print('number_of_vertices=', number_of_vertices)
  print('number_of_edges=', number_of_edges)
  density = [number_of_edges[i]*2.0/(number_of_vertices[i]*(number_of_vertices[i]-1)) for i in range(len(number_of_vertices))]
  print('density=', density)


input_stats('../graphs/input/input', 16)
input_stats('../graphs/input18/input', 14)


