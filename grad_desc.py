import numpy as np
from scipy.optimize import minimize

def stress(X, weights, distances, n):
	print "Parameters:", X, type(X)
	s = 0.0
	for i in range(0,n):
		for j in range(i+1,n):
			norm = X[i,:] - X[j,:]
			norm = np.sqrt(sum(pow(norm,2)))
			s += weights[i,j] * pow((norm - distances[i,j]), 2)
	return s

#x0 = 

# res = minimize(rosen, x0, method='BFGS', jac = grad_func, options={'disp': True})	
res = minimize(rosen, x0, method='BFGS', options={'disp': True})

print(res.x)