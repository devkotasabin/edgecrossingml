import numpy as np
import sys
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from input_functions import *
from edge_crossing import *

my_inf = 10000000

def usage():
 if len(sys.argv)<3:
  print('usage:python plot_main.py folder_name/ part_of_file_name/A')
  quit()
 else:
  return sys.argv[1], sys.argv[2]

folder_name, part_of_file_name = usage()

def plot_cost(nW, costs_of_min_crossings, costs_of_neato_crossings):
 print('Cost:')
 er_10_4_cost = np.load(folder_name + 'er_10_0.4_cost.npy')
 #print(er_10_4_cost)
 print(er_10_4_cost.shape)
 for w in range(nW):
  print('costs_of_min_crossings:')
  print(costs_of_min_crossings)
  fig = plt.figure()
  plt.plot(range(1,nK+1), costs_of_min_crossings, 'r--', label='random')
  plt.plot(range(1,nK+1), costs_of_neato_crossings, 'bs', label='neato')
  plt.legend(loc=1)
  plt.title(caption)
  plt.ylabel('Cost')
  plt.xlabel('log(K)')
  plt.savefig(folder_name+'er_10_4_cost_'+str(w)+'.png')

def plot_ncr(part_of_file_name, n):
 global my_inf
 overall_min_crossing = my_inf
 minW = 0
 minK = 0
 minIter = 0
 neato_wins = 0
 normalize_str = '_wo_norm'
 if n==1:
  normalize_str = '_norm'
 if n == -1:
  normalize_str = ''
 #print('Number of crossings:')
 arr_ncr = np.load(folder_name + part_of_file_name + '_ncr'+normalize_str+'.npy')
 #print(arr_ncr)
 #print(arr_ncr.shape)
 #print('Cost:')
 arr_cost = np.load(folder_name + part_of_file_name + '_cost'+normalize_str+'.npy')
 #print(arr_cost)
 #print(arr_cost.shape)
 nW, nK, nIter = arr_ncr.shape
 fig_number = 0
 for w in range(nW):
  caption = 'W='+str(w)+', One random graph, ' + part_of_file_name
  random_min_crossings = []
  random_max_crossings = []
  neato_min_crossings = []
  neato_max_crossings = []
  costs_of_min_crossings = []
  costs_of_neato_crossings = []
  for k in range(nK):
   random_min_crossings.append(my_inf)
   random_max_crossings.append(-1)
   neato_min_crossings.append(my_inf)
   neato_max_crossings.append(-1)
   costs_of_min_crossings.append(0)
   costs_of_neato_crossings.append(0)
   for i in range(nIter):
    if i<(nIter/2):
     #neato_crossings[k] = arr_ncr[w][k][i]
     #costs_of_neato_crossings[k] = arr_cost[w][k][i][0]
     if arr_ncr[w][k][i] < neato_min_crossings[k]:
      #neato_min_crossings[k] = arr_ncr[w][k][i]
      # crossing should not be fraction, but for input 1 it is fraction!!!!
      # temporary fix, cast to int
      neato_min_crossings[k] = int(arr_ncr[w][k][i])
      if overall_min_crossing > neato_min_crossings[k]:
       overall_min_crossing = neato_min_crossings[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 1
     if arr_ncr[w][k][i] > neato_max_crossings[k]:
      #neato_max_crossings[k] = arr_ncr[w][k][i]
      neato_max_crossings[k] = int(arr_ncr[w][k][i])
    else:
     if arr_ncr[w][k][i] < random_min_crossings[k]:
      #random_min_crossings[k] = arr_ncr[w][k][i]
      random_min_crossings[k] = int(arr_ncr[w][k][i])
      costs_of_min_crossings[k] = arr_cost[w][k][i][0]
      if overall_min_crossing > random_min_crossings[k]:
       overall_min_crossing = random_min_crossings[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 0
     if arr_ncr[w][k][i] > random_max_crossings[k]:
      #random_max_crossings[k] = arr_ncr[w][k][i]
      random_max_crossings[k] = int(arr_ncr[w][k][i])
     #print('i:'+str(i)+',crossing:'+str(arr_ncr[w][k][i])+',min:'+str(random_min_crossings[k])+',max:'+str(random_max_crossings[k]))
  #print('min_crossings:')
  #print(random_min_crossings)
  #print('max crossings:')
  #print(random_max_crossings)
  fig = plt.figure(fig_number)
  fig_number = fig_number + 1
  plt.plot(range(-int(nK/2),int(nK/2)+1), random_min_crossings, 'r--', label='random min')
  plt.plot(range(-int(nK/2),int(nK/2)+1), random_max_crossings, 'go', label='random max')
  plt.plot(range(-int(nK/2),int(nK/2)+1), neato_min_crossings, 'bs', label='neato min')
  plt.plot(range(-int(nK/2),int(nK/2)+1), neato_max_crossings, 'y^', label='neato max')
  plt.legend(loc=1)
  plt.title(caption)
  plt.ylabel('Number of crossings')
  plt.xlabel('log(K)')
  plt.savefig(folder_name+part_of_file_name+'_ncr_'+str(w)+normalize_str+'.png')
  plt.close(fig)
  #print('costs_of_min_crossings:')
  #print(costs_of_min_crossings)
  #print('costs_of_neato_crossings:')
  #print(costs_of_neato_crossings)
  #fig = plt.figure(fig_number)
  #fig_number = fig_number + 1
  #plt.plot(range(1,nK+1), costs_of_min_crossings, 'r--', label='random')
  #plt.plot(range(1,nK+1), costs_of_neato_crossings, 'bs', label='neato')
  #plt.legend(loc=1)
  #plt.title(caption)
  #plt.ylabel('Cost')
  #plt.xlabel('log(K)')
  #plt.savefig(folder_name+part_of_file_name+'_cost_'+str(w)+'.png')
  #plt.close(fig)
 costs = []
 nSteps = arr_cost.shape[3]
 for s in range(nSteps):
  costs.append(arr_cost[minW][minK][minIter][s])
 #print('Costs:')
 #print(costs)
 fig = plt.figure(fig_number)
 fig_number = fig_number + 1
 plt.plot(range(1,nSteps+1), costs)
 plt.title('W='+str(minW)+', K='+str(minK)+', Layout='+str(minIter))
 plt.ylabel('Cost')
 plt.xlabel('Iterations')
 plt.savefig(folder_name+part_of_file_name+'_cost.png')
 plt.close(fig)
 #print('overall_min_crossing: '+str(overall_min_crossing))
 return minW, minK, minIter, neato_wins, overall_min_crossing

def find_min_angle(G, X):
 edge_list = G.edges()
 m = len(edge_list)
 # for every edge in the graph
 # draw a line joining the endpoints
 for i in range(0,m):
  i1, i2 = getNodesforEdge(i, edge_list)
  A = np.zeros((2,2))

  A[0,:] = X[i1, :]
  A[1,:] = X[i2, :]
  plt.plot(A[:,0] , A[:,1], color='blue')

 num_intersections = 0
 min_angle = math.pi/2.0

 print("Number of edges: ", m)

 # loop through all edge pairs
 for i in range(0,m):
  for j in range(i+1,m):

   A,B = getEdgePairAsMatrix(X,i,j,edge_list)
   # doIntersect(x11, y11, x12, y12, x21, y21, x22, y22)
   if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):

    # print A
    # print B
    # print i
    # print j
    x_pt, y_pt = getIntersection(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])
    theta = getAngleLineSeg(A[0][0], A[0][1], B[0][0], B[0][1], x_pt, y_pt)
    if theta > math.pi/2.0:
     theta = math.pi - theta
    if theta < min_angle:
     min_angle = theta

    num_intersections += 1

 print("Number of Edge Crossings: ", num_intersections)
 print("Minimum Angle: ", to_deg(min_angle))
 return to_deg(min_angle)

def plot_angle(part_of_file_name, n):
 global my_inf
 overall_min_crossing = my_inf
 minW = 0
 minK = 0
 minIter = 0
 neato_wins = 0
 normalize_str = '_wo_norm'
 if n==1:
  normalize_str = '_norm'
 if n == -1:
  normalize_str = ''
 #print('Number of crossings:')
 arr_ncr = np.load(folder_name + part_of_file_name + '_ncr'+normalize_str+'.npy')
 #print(arr_ncr)
 #print(arr_ncr.shape)
 arr_xy = np.load(folder_name + part_of_file_name + '_xy'+normalize_str+'.npy')
 G = build_networkx_graph(folder_name + part_of_file_name + '.txt')
 #print('Cost:')
 arr_cost = np.load(folder_name + part_of_file_name + '_cost'+normalize_str+'.npy')
 #print(arr_cost)
 #print(arr_cost.shape)
 nW, nK, nIter = arr_ncr.shape
 fig_number = 0
 for w in range(nW):
  caption = 'W='+str(w)+', One random graph, ' + part_of_file_name
  random_min_angle = []
  random_max_angle = []
  neato_min_angle = []
  neato_max_angle = []
  costs_of_min_crossings = []
  costs_of_neato_crossings = []
  for k in range(nK):
   random_min_angle.append(180)
   random_max_angle.append(-1)
   neato_min_angle.append(180)
   neato_max_angle.append(-1)
   costs_of_min_crossings.append(0)
   costs_of_neato_crossings.append(0)
   for i in range(nIter):
    if i<(nIter/2):
     #neato_crossings[k] = arr_ncr[w][k][i]
     #costs_of_neato_crossings[k] = arr_cost[w][k][i][0]
     if find_min_angle(G, arr_xy[w][k][i]) < neato_min_angle[k]:
      #neato_min_crossings[k] = arr_ncr[w][k][i]
      # crossing should not be fraction, but for input 1 it is fraction!!!!
      # temporary fix, cast to int
      neato_min_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
      if overall_min_crossing > neato_min_angle[k]:
       overall_min_crossing = neato_min_angle[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 1
     if find_min_angle(G, arr_xy[w][k][i]) > neato_max_angle[k]:
      #neato_max_crossings[k] = arr_ncr[w][k][i]
      neato_max_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
    else:
     if find_min_angle(G, arr_xy[w][k][i]) < random_min_angle[k]:
      #random_min_crossings[k] = arr_ncr[w][k][i]
      random_min_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
      costs_of_min_crossings[k] = arr_cost[w][k][i][0]
      if overall_min_crossing > random_min_angle[k]:
       overall_min_crossing = random_min_angle[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 0
     if find_min_angle(G, arr_xy[w][k][i]) > random_max_angle[k]:
      #random_max_crossings[k] = arr_ncr[w][k][i]
      random_max_angle[k] = int(find_min_angle(G, arr_xy[w][k][i]))
     #print('i:'+str(i)+',crossing:'+str(arr_ncr[w][k][i])+',min:'+str(random_min_crossings[k])+',max:'+str(random_max_crossings[k]))
  #print('min_crossings:')
  #print(random_min_crossings)
  #print('max crossings:')
  #print(random_max_crossings)
  fig = plt.figure(fig_number)
  fig_number = fig_number + 1
  plt.plot(range(-int(nK/2),int(nK/2)+1), random_min_angle, 'r--', label='random min')
  plt.plot(range(-int(nK/2),int(nK/2)+1), random_max_angle, 'go', label='random max')
  plt.plot(range(-int(nK/2),int(nK/2)+1), neato_min_angle, 'bs', label='neato min')
  plt.plot(range(-int(nK/2),int(nK/2)+1), neato_max_angle, 'y^', label='neato max')
  plt.legend(loc=1)
  plt.title(caption)
  plt.ylabel('Crossing angle')
  plt.xlabel('log(K)')
  plt.savefig(folder_name+part_of_file_name+'_angle_'+str(w)+normalize_str+'.png')
  plt.close(fig)
  #print('costs_of_min_crossings:')
  #print(costs_of_min_crossings)
  #print('costs_of_neato_crossings:')
  #print(costs_of_neato_crossings)
  #fig = plt.figure(fig_number)
  #fig_number = fig_number + 1
  #plt.plot(range(1,nK+1), costs_of_min_crossings, 'r--', label='random')
  #plt.plot(range(1,nK+1), costs_of_neato_crossings, 'bs', label='neato')
  #plt.legend(loc=1)
  #plt.title(caption)
  #plt.ylabel('Cost')
  #plt.xlabel('log(K)')
  #plt.savefig(folder_name+part_of_file_name+'_cost_'+str(w)+'.png')
  #plt.close(fig)
 costs = []
 nSteps = arr_cost.shape[3]
 for s in range(nSteps):
  costs.append(arr_cost[minW][minK][minIter][s])
 #print('Costs:')
 #print(costs)
 fig = plt.figure(fig_number)
 fig_number = fig_number + 1
 plt.plot(range(1,nSteps+1), costs)
 plt.title('W='+str(minW)+', K='+str(minK)+', Layout='+str(minIter))
 plt.ylabel('Cost')
 plt.xlabel('Iterations')
 plt.savefig(folder_name+part_of_file_name+'_cost.png')
 plt.close(fig)
 #print('overall_min_crossing: '+str(overall_min_crossing))
 return minW, minK, minIter, neato_wins, overall_min_crossing

# it was for old version, K = 10, 100, 1000, 10000, 100000
def plot_ncr_input(part_of_file_name, n):
 global my_inf
 overall_min_crossing = my_inf
 minW = 0
 minK = 0
 minIter = 0
 neato_wins = 0
 normalize_str = '_wo_norm'
 if n==1:
  normalize_str = '_norm'
 if n == -1:
  normalize_str = ''
 #print('Number of crossings:')
 arr_ncr = np.load(folder_name + part_of_file_name + '_ncr'+normalize_str+'.npy')
 #print(arr_ncr)
 #print(arr_ncr.shape)
 #print('Cost:')
 arr_cost = np.load(folder_name + part_of_file_name + '_cost'+normalize_str+'.npy')
 #print(arr_cost)
 #print(arr_cost.shape)
 nW, nK, nIter = arr_ncr.shape
 fig_number = 0
 for w in range(nW):
  caption = 'W='+str(w+1)+', ' + part_of_file_name
  random_min_crossings = []
  random_max_crossings = []
  neato_min_crossings = []
  neato_max_crossings = []
  input_crossing = []
  costs_of_min_crossings = []
  costs_of_neato_crossings = []
  for k in range(nK):
   random_min_crossings.append(my_inf)
   random_max_crossings.append(-1)
   neato_min_crossings.append(my_inf)
   neato_max_crossings.append(-1)
   costs_of_min_crossings.append(0)
   costs_of_neato_crossings.append(0)
   input_crossing.append(0)
   for i in range(nIter):
    if i==0:
     input_crossing[k] == arr_ncr[w][k][i]
    elif i<=(nIter/2):
     #neato_crossings[k] = arr_ncr[w][k][i]
     #costs_of_neato_crossings[k] = arr_cost[w][k][i][0]
     if arr_ncr[w][k][i] < neato_min_crossings[k]:
      neato_min_crossings[k] = arr_ncr[w][k][i]
      if overall_min_crossing > neato_min_crossings[k]:
       overall_min_crossing = neato_min_crossings[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 1
     if arr_ncr[w][k][i] > neato_max_crossings[k]:
      neato_max_crossings[k] = arr_ncr[w][k][i]
    else:
     if arr_ncr[w][k][i] < random_min_crossings[k]:
      random_min_crossings[k] = arr_ncr[w][k][i]
      costs_of_min_crossings[k] = arr_cost[w][k][i][0]
      if overall_min_crossing > random_min_crossings[k]:
       overall_min_crossing = random_min_crossings[k]
       minW = w
       minK = k
       minIter = i
       neato_wins = 0
     if arr_ncr[w][k][i] > random_max_crossings[k]:
      random_max_crossings[k] = arr_ncr[w][k][i]
     #print('i:'+str(i)+',crossing:'+str(arr_ncr[w][k][i])+',min:'+str(random_min_crossings[k])+',max:'+str(random_max_crossings[k]))
  #print('min_crossings:')
  #print(random_min_crossings)
  #print('max crossings:')
  #print(random_max_crossings)
  fig = plt.figure(fig_number)
  fig_number = fig_number + 1
  plt.plot([1, 2, 3, 4, 5], random_min_crossings, 'r--', label='random min')
  plt.plot([1, 2, 3, 4, 5], random_max_crossings, 'go', label='random max')
  plt.plot([1, 2, 3, 4, 5], neato_min_crossings, 'bs', label='neato min')
  plt.plot([1, 2, 3, 4, 5], neato_max_crossings, 'y^', label='neato max')
  plt.plot([1, 2, 3, 4, 5], input_crossing, 'y--', label='input')
  plt.legend(loc=1)
  plt.title(caption)
  plt.ylabel('Number of crossings')
  plt.xlabel('log(K)')
  plt.savefig(folder_name+part_of_file_name+'_ncr_'+str(w)+normalize_str+'.png')
  plt.close(fig)
  #print('costs_of_min_crossings:')
  #print(costs_of_min_crossings)
  #print('costs_of_neato_crossings:')
  #print(costs_of_neato_crossings)
  #fig = plt.figure(fig_number)
  #fig_number = fig_number + 1
  #plt.plot(range(1,nK+1), costs_of_min_crossings, 'r--', label='random')
  #plt.plot(range(1,nK+1), costs_of_neato_crossings, 'bs', label='neato')
  #plt.legend(loc=1)
  #plt.title(caption)
  #plt.ylabel('Cost')
  #plt.xlabel('log(K)')
  #plt.savefig(folder_name+part_of_file_name+'_cost_'+str(w)+'.png')
  #plt.close(fig)
 costs = []
 nSteps = arr_cost.shape[3]
 for s in range(nSteps):
  costs.append(arr_cost[minW][minK][minIter][s])
 #print('Costs:')
 #print(costs)
 fig = plt.figure(fig_number)
 fig_number = fig_number + 1
 plt.plot(range(1,nSteps+1), costs)
 plt.title('W='+str(minW)+', K='+str(minK)+', Layout='+str(minIter))
 plt.ylabel('Cost')
 plt.xlabel('Iterations')
 plt.savefig(folder_name+part_of_file_name+'_cost.png')
 plt.close(fig)
 #print('overall_min_crossing: '+str(overall_min_crossing))
 return minW, minK, minIter, neato_wins, overall_min_crossing




#This function returns the nodes of an edge given its index in the edge list
def getNodesforEdge(index, edge_list):
        #print(index)
        #print(edge_list)
        #print('type(edge_list)')
        #print(type(edge_list))
        #print('type(edge_list[index])')
        #print(type(edge_list[index]))
        #print(str(edge_list[index]))
        #print(str(edge_list[index][0]))
        return edge_list[index][0], edge_list[index][1]


# This function extracts the edge pair in the form of matrices
# Returns two matrices A and B
# A contains [a1x, a1y; a2x a2y]
# B contains [b1x, b1y; b2x b2y]
def getEdgePairAsMatrix(X,i,j,edge_list):
        A = np.zeros((2,2))
        B = np.zeros((2,2))

        i1, i2 = getNodesforEdge(i,edge_list)
        j1, j2 = getNodesforEdge(j,edge_list)

        A[0,:] = X[i1, :]
        A[1,:] = X[i2, :]

        B[0,:] = X[j1, :]
        B[1,:] = X[j2, :]

        return A,B


def num_crossings(G,X):
 num_intersections = 0

 # print X

 edge_list = G.edges()
 m = len(edge_list)
 # print "Number of edges: ", m

 # loop through all edge pairs
 for i in range(0,m):
  for j in range(i+1,m):

   A,B = getEdgePairAsMatrix(X,i,j,edge_list)
   # doIntersect(x11, y11, x12, y12, x21, y21, x22, y22)
   if(doIntersect(A[0][0], A[0][1], A[1][0], A[1][1], B[0][0], B[0][1], B[1][0], B[1][1])):

    num_intersections += 1

 return num_intersections

def plot_ncr_initial_vs_final(part_of_file_name, n):
 global my_inf
 overall_min_crossing = my_inf
 minW = 0
 minK = 0
 minIter = 0
 neato_wins = 0
 normalize_str = '_wo_norm'
 if n==1:
  normalize_str = '_norm'
 if n == -1:
  normalize_str = ''
 #print('Number of crossings:')
 arr_ncr = np.load(folder_name + part_of_file_name + '_ncr'+normalize_str+'.npy')
 #print(arr_ncr)
 #print(arr_ncr.shape)
 arr_xy = np.load(folder_name + part_of_file_name + '_init_xy'+normalize_str+'.npy')
 #print('Cost:')
 arr_cost = np.load(folder_name + part_of_file_name + '_cost'+normalize_str+'.npy')
 #print(arr_cost)
 #print(arr_cost.shape)
 G = build_networkx_graph(folder_name + part_of_file_name + '.txt')
 nW, nK, nIter = arr_ncr.shape
 fig_number = 0
 for w in range(nW):
  for k in range(nK):
   caption = 'W='+str(w)+', K='+str(k)+', ' + part_of_file_name
   initial_crossings = []
   final_crossings = []
   for i in range(nIter):
    #we don't have initial crossing, but may be we can compute it from xy array
    initial_crossings.append(num_crossings(G, arr_xy[w][k][i]))
    final_crossings.append(arr_ncr[w][k][i])
   #print('min_crossings:')
   #print(random_min_crossings)
   #print('max crossings:')
   #print(random_max_crossings)
   fig = plt.figure(fig_number)
   fig_number = fig_number + 1
   plt.plot(range(nIter), initial_crossings, 'r--', label='initial')
   plt.plot(range(nIter), final_crossings, 'go', label='final')
   plt.legend(loc=1)
   plt.title(caption)
   plt.ylabel('Number of crossings')
   plt.xlabel('Iterations')
   plt.savefig(folder_name+'init_vs_final/'+part_of_file_name+'_ncr_init_vs_final_w_'+str(w)+'k_'+str(k)+normalize_str+'.png')
   plt.close(fig)
 #print('overall_min_crossing: '+str(overall_min_crossing))
 return minW, minK, minIter, neato_wins, overall_min_crossing


def plot_xy(n):
 print('********************************************')
 norm_str = '_wo_norm'
 if n==1:
  norm_str = '_norm'
 print('XY coordinates:')
 #er_10_4_xy = np.load(folder_name + 'er_10_0.4_xy.npy')
 input1_xy = np.load(folder_name + 'input1_xy'+norm_str+'.npy')
 #print(er_10_4_xy)
 #print(er_10_4_xy.shape)
 print(input1_xy.shape)

def plot_listed_file(file_name_prefix, normalized):
 global my_inf
 W_start = 0
 W_end = 2

 K_start = -5
 K_end = 6

 neato_random = [0, 0]

 normalize_count = [0, 0]

 W_count = [0 for i in range(W_end - W_start)]
 logK_count = [0 for i in range(K_end - K_start)]
 for i in range(len(file_name_prefix)):
  if normalized[i]==0:continue
  norm_min = my_inf
  norm_win = 0
  for n in range(2):
   #plot_ncr_initial_vs_final(file_name_prefix[i], n)
   minW, minK, minIter, neato_wins, overall_min_crossing = plot_ncr(file_name_prefix[i], n)
   minW, minK, minIter, neato_wins, overall_min_crossing = plot_angle(file_name_prefix[i], n)
   if norm_min > overall_min_crossing:
    norm_min = overall_min_crossing
    if n==0:
     norm_win = 0
    else:
     norm_win = 1
   W_count[minW] = W_count[minW] + 1
   logK_count[minK] = logK_count[minK] + 1
   if neato_wins==1:
    neato_random[0] = neato_random[0]+1
   else:
    neato_random[1] = neato_random[1]+1
   if norm_win == 1:
    normalize_count[1] = normalize_count[1]+1
   else:
    normalize_count[0] = normalize_count[0]+1

 print('W count:')
 print(range(W_end - W_start))
 print(W_count)

 print('logK count:')
 print([i for i in range(K_start, K_end)])
 print(logK_count)

 print('NEATO wins: '+str(neato_random[0]))
 print('RANDOM wins: '+str(neato_random[1]))

 print('Normalization wins: '+str(normalize_count[1]))
 print('Normalization losses: '+str(normalize_count[0]))

 W_start = 0
 W_end = 2

 K_start = -5
 K_end = 6

 neato_random = [0, 0]

 W_count = [0 for i in range(W_end - W_start)]
 logK_count = [0 for i in range(K_end - K_start)]
 for i in range(len(file_name_prefix)):
  if normalized[i]==1:continue
  minW, minK, minIter, neato_wins, overall_min_crossing = plot_ncr(file_name_prefix[i], 0)
  minW, minK, minIter, neato_wins, overall_min_crossing = plot_angle(file_name_prefix[i], 0)
  W_count[minW] = W_count[minW] + 1
  logK_count[minK] = logK_count[minK] + 1
  if neato_wins==1:
   neato_random[0] = neato_random[0]+1
  else:
   neato_random[1] = neato_random[1]+1

 print('W count:')
 print(range(W_end - W_start))
 print(W_count)

 print('logK count:')
 print([i for i in range(K_start, K_end)])
 print(logK_count)

 print('NEATO wins: '+str(neato_random[0]))
 print('RANDOM wins: '+str(neato_random[1]))

#plot_cost()
if part_of_file_name!='A':
 #plot_ncr(part_of_file_name, 1)
 plot_ncr_input(part_of_file_name, -1)
else:
 #plot_listed_file(['er_5_0.6', 'er_6_0.6', 'er_7_0.6', 'er_8_0.6', 'er_10_0.6', 'er_15_0.4', 'er_15_0.5', 'er_17_0.4'], [1, 1, 1, 1, 1, 0, 0, 0])
 #plot_listed_file(['er_17_0.4'], [0])
 #plot_listed_file(['input1', 'input2', 'input3', 'input4'], [1, 1, 1, 1])
 plot_listed_file(['input1', 'input2', 'input3'], [1, 1, 1])


#plot_xy(0)

