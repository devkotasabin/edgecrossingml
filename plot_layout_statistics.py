def parse_dot_file(file_name):
 file = open(file_name, 'r')
 arr = file.read().split(';')
 nodes = []
 edges = []
 node_coords = []
 edge_list = []
 for i in range(len(arr)):
 #for i in range(10):
  arr[i] = arr[i].strip()
  elmnt = arr[i].split()
  if len(elmnt)>2:
   if elmnt[1][0]=='[':
    #print(elmnt[0])
    nodes.append(elmnt)
   else:
    #print(elmnt[0]+elmnt[1]+elmnt[2])
    edges.append(elmnt)
  #print(elmnt)
 for i in range(len(nodes)):
  #print nodes[i]
  #print(nodes[i][0])
  coords = nodes[i][2][5:len(nodes[i][2])-2].split(',')
  for k in range(len(coords)):
   coords[k] = float(coords[k])
  node_coords.append(coords)
 for i in range(1,len(edges)):
  edg = []
  #print(edges[i])
  edg.append(int(edges[i][0]))
  edg.append(int(edges[i][2]))
  edge_list.append(edg)
 file.close()

 #print(node_coords)
 #print(edge_list)
 return node_coords, edge_list

# this function used to find out whether two vertices share or not
# but now it is redundant as the computation is already placed in edgecrossing.py inside the doIntersect function
def share_vertex(e1, e2):
 if e1[0]==e2[0] or e1[0]==e2[1]:
  return True
 if e1[1]==e2[0] or e1[1]==e2[1]:
  return True
 return False

from edge_crossing import *

def number_of_intersections(file_name):
 node_coords, edge_list = parse_dot_file(file_name)
 count = 0
 for i in range(len(edge_list)):
  for j in range(i+1,len(edge_list)):
   edge1 = edge_list[i]
   edge2 = edge_list[j]
   #if not share_vertex(edge1, edge2):
   if(doIntersect(node_coords[edge1[0]][0], node_coords[edge1[0]][1], node_coords[edge1[1]][0], node_coords[edge1[1]][1], node_coords[edge2[0]][0], node_coords[edge2[0]][1], node_coords[edge2[1]][0], node_coords[edge2[1]][1])):
    count = count + 1
    #print(str(edge1)+" intersects "+str(edge2))
 return count

def all_graph_crossing_numbers(algo_name, folder_name, number_of_graphs):
 arr = []
 for i in range(1,number_of_graphs+1):
 #for i in range(1,3):
  #print(folder_name+'/'+algo_name+'_layout'+str(i)+'.dot')
  #print('number of intersections:')
  #print(number_of_intersections(folder_name+'/'+algo_name+'_layout'+str(i)+'.dot'))
  arr.append(number_of_intersections(folder_name+'/'+algo_name+'_layout'+str(i)+'.dot'))
 return arr

def all_drawings(algo_name, folder_name, number_of_graphs, number_of_drawings):
 arr = []
 for i in range(number_of_drawings):
  arr.append(all_graph_crossing_numbers(algo_name, folder_name, number_of_graphs))
 return arr

import json
import sys

def usage():
 if len(sys.argv)<4:
  print('usage:python plot_layout_statistics.py folder_name number_of_graphs number_of_drawings')
  quit()
 else:
  return sys.argv[1], int(sys.argv[2]), int(sys.argv[3])

#file = open('sfdp_crossings2.txt','w')
#file.write(json.dumps(all_graph_crossing_numbers('sfdp', 'erdos_renyi2')))
#file.close()

#file = open('neato_crossings2.txt','w')
#file.write(json.dumps(all_graph_crossing_numbers('neato', 'erdos_renyi2')))
#file.close()

#print(json.dumps(number_of_intersections('test_graphs/sfdp_layout_complete5.dot')))

folder_name, number_of_graphs, number_of_drawings = usage()

file = open(folder_name+'/'+'sfdp_crossings.txt','w')
file.write(json.dumps(all_drawings('sfdp', folder_name, number_of_graphs, number_of_drawings)))
file.close()

file = open(folder_name+'/'+'neato_crossings.txt','w')
file.write(json.dumps(all_drawings('neato', folder_name, number_of_graphs, number_of_drawings)))
file.close()

