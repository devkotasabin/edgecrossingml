Edge Crossing Reduction using Machine Learning https://www.overleaf.com/16217464pymgsvfbczxc#/62051475/ based on the paper https://cseweb.ucsd.edu/~lvdmaaten/workshops/nips2010/papers/bennett.pdf

Possible Ways to improve

- Explore different stoppage criteria (number of iterations or cost value)
- Get an insight on how the penalty changes for an edge pair. And why it ranges between 1 and 2 and how it varies in between.
