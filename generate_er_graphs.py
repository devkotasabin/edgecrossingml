import networkx as nx
import random


def generate_er(file_name, n, p):
 file = open(file_name,"w")
 graph = nx.generators.random_graphs.erdos_renyi_graph(n, p)
 file.write(str(graph.number_of_nodes())+"\n");
 for j in range(graph.number_of_nodes()):
  file.write(str(random.randint(1,300))+" "+str(random.randint(1,300))+"\n")
 edges = graph.edges()
 for e in edges:
  file.write(str(e[0])+" "+str(e[1])+"\n")
 file.close()

#folder_name = "meeting_08_21_2018/todo2/"

folder_name = "profile_smart_algorithm/"

#file_names = ["er_8_0.6", "er_10_0.6", "er_15_0.4", "er_15_0.5", "er_17_0.4", "er_20_0.3", "er_20_0.4", "er_25_0.3", "er_25_0.4", "er_30_0.3"]
#nodes = [8, 10, 15, 15, 17, 20, 20, 25, 25, 30]
#probs = [0.6, 0.6, 0.4, 0.5, 0.4, 0.3, 0.4, 0.3, 0.4, 0.3]

#file_names = ["er_5_0.6", "er_6_0.6", "er_7_0.6"]
#nodes = [5, 6, 7]
#probs = [0.6, 0.6, 0.6]


file_names = ["er_50_0.1", "er_50_0.2", "er_100_0.1", "er_100_0.2", "er_150_0.1", "er_150_0.2", "er_200_0.1", "er_200_0.2"]
nodes = [50, 50, 100, 100, 150, 150, 200, 200]
probs = [0.1, 0.2, 0.1, 0.2, 0.1, 0.2, 0.1, 0.2]


for i in range(len(file_names)):
 generate_er(folder_name+file_names[i]+".txt", nodes[i], probs[i])

